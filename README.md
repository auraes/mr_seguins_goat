**Mr Seguin's Goat**  
Version 1.2  
Auraes, 2023-2025.  
Licence Creative Commons BY-NC-ND 4.0 (CC BY-NC-ND 4.0 FR)  
https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr

**Mr Seguin's Goat** est un jeu d’aventure textuel réalisé à l’occasion de Text Adventure Literacy Jam, 2023.  

Le jeu est téléchargable à l’adresse suivante :  
https://auraes.itch.io/mr-seguins-goat

Text Adventure Literacy Jam 2023 :  
https://itch.io/jam/talp2023  

