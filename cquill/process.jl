#define FLAG_DARK        0
#define FLAG_YESNO       5
#define FLAG_PUTIN       6

#define FLAG_START       11
#define FLAG_CORDE       12
#define FLAG_EAT         13
#define FLAG_OBEDIENT    14
#define FLAG_SEGUIN      15
#define FLAG_LISTEN      16

#define FLAG_PLAY        19
#define FLAG_TURN        20
#define FLAG_MAX         21
#define FLAG_INV         22
#define FLAG_SCORE_PLUS  23
#define FLAG_NCARDS      24
#define FLAG_CARD1       25
#define FLAG_CARD2       26
#define FLAG_CARD3       27
#define FLAG_CARD4       28
#define FLAG_CARD5       29
#define FLAG_SCORE       30

#define FLAG_BARREL      12
#define FLAG_SALT        13
#define FLAG_LICK        14

#define FLAG_SHORTCUT    12
#define FLAG_RABBIT      13
#define FLAG_SNAKE       14
#define FLAG_JINGLE      15
#define FLAG_STORM       16

#define FLAG_BOULDER     13
#define FLAG_PATH        14
#define FLAG_NOBODY      15
#define FLAG_CAMPFIRE    16
#define FLAG_LAMB        17


@none

@any

@status
at room0
set FLAG_CORDE
let FLAG_EAT 4
anykey
goto room4
desc

at room4
zero FLAG_START
let FLAG_START 1
destroy seguin
"\nSeguin: Hi Blanquette, my pretty young goat, come and see me in the stable when you have found and eaten a vegetable, a flower and a fruit that will make your milk fat and fragrant."
anykey
desc

at room4
eq FLAG_START 1
set FLAG_START
"\n[Type HELP if this is your first time playing this game.]"

at room4
notzero FLAG_CORDE
set FLAG_CORDE

at room10
notzero FLAG_CORDE
"\nSeguin: Hey! Blanquette, come back to me when you have eaten a vegetable, a flower and a fruit."
anykey
goto room7
desc

eq FLAG_EAT 1
clear FLAG_EAT
place seguin_milking room4
swap door_opened door_locked

at room12
notzero FLAG_BARREL
clear FLAG_SHORTCUT
clear FLAG_RABBIT
clear FLAG_SNAKE
clear FLAG_JINGLE
clear FLAG_LISTEN
place door_locked room16
"Blanquette climbs onto the barrel, and jumps out of the window."
anykey
goto room13
desc

at room16
zero FLAG_SNAKE
place snake room17

at room18
zero FLAG_SNAKE
place snake room17

at room18
zero FLAG_RABBIT
place rabbit_leisurely room16

eq FLAG_STORM 1
at room16
set FLAG_STORM
swap elves statue
cls
"A violent storm suddenly breaks out, pouring a torrential rain that makes the river overflow, washing away part of the bridge that crosses it.\n\nThe party is over, the elves have gone home."
anykey
desc

at room21
eq FLAG_CAMPFIRE 3
clear FLAG_CAMPFIRE
destroy hermit
swap ashes campfire
destroy lamb_1
"\nHermit: I've got everything I need. See you later."
anykey
desc

at room29
eq FLAG_NCARDS 4
set FLAG_MAX

at room29
goto room30
set FLAG_TURN
anykey
desc

atgt room29
notzero FLAG_TURN
clear FLAG_TURN
"\nIt's your turn Blanquette, choose a card from 1 to 4 in your deck."

at room30
eq FLAG_PLAY 1
"\nThe mongoose fends off Kitten's attack."
"\nNo one won the round."

at room30
eq FLAG_PLAY 2
"\nThe mongoose is poisoned by the mushroom."
"\nBlanquette won the round."

at room30
eq FLAG_PLAY 3
"\nA 100 t hammer comes out of the joker card and hits the wolf."
"\nBlanquette won the round."

at room30
eq FLAG_PLAY 4
"\nThe snake is eaten by the mongoose."
"\nBlanquette lost the round."

at room30
notzero FLAG_PLAY
clear FLAG_PLAY
set FLAG_TURN
goto room31
anykey
desc

at room31
eq FLAG_PLAY 1
"\nThe bird of prey grabs and carries off Kitten."
"\nBlanquette lost the round."

at room31
eq FLAG_PLAY 2
"\nThe bird of prey ignores the mushroom."
"\nNo one won the round."

at room31
eq FLAG_PLAY 3
"\nA 100 t hammer comes out of the joker card and hits the wolf."
"\nBlanquette won the round."

at room31
eq FLAG_PLAY 4
"\nThe snake is eaten by the bird of prey!"
"\nBlanquette lose the round."

at room31
notzero FLAG_PLAY
clear FLAG_PLAY
set FLAG_TURN
goto room32
anykey
desc

at room32
eq FLAG_PLAY 1
"\nThe joker dodges the attack."
"\nNo one won the round."

at room32
eq FLAG_PLAY 2
"\nThe joker dodges the attack."
"\nNo one won the round."

at room32
eq FLAG_PLAY 3
"\nThe joker dodges the attack."
"\nNo one won the round."

at room32
eq FLAG_PLAY 4
"\nThe joker dodges the attack."
"\nNo one won the round."

at room32
notzero FLAG_PLAY
clear FLAG_PLAY
set FLAG_TURN
goto room33
anykey
desc

at room33
eq FLAG_PLAY 1
"\nThe kitten lacerates the ectoplasm with its claws."
"\nBlanquette won the round."

at room33
eq FLAG_PLAY 2
"\nThe ectoplasm feasts on the mushroom."
"\nBlanquette lost the round."

at room33
eq FLAG_PLAY 3
"\nA 100 t hammer comes out of the joker card and hits the wolf."
"\nBlanquette won the round."

at room33
eq FLAG_PLAY 4
"\nThe ectoplasm dodges the snake's attack."
"\nNo one won the round."

at room33
notzero FLAG_PLAY
goto room34
anykey
desc

atgt room29
zero FLAG_NCARDS
set FLAG_NCARDS
goto room34
desc

at room34
notzero FLAG_MAX
goto room35
anykey
desc

at room34
goto room36
anykey
desc

at room35
score
turns
end

at room36
score
turns
end

notzero FLAG_SCORE_PLUS
clear FLAG_SCORE_PLUS
"\n[The score has gone up by 10 points.]"
plus FLAG_SCORE 10

eq FLAG_INV 1
set FLAG_INV
"\nType X DECK to display the contents of your deck."

@n
'*'
notzero FLAG_YESNO
at room11
present kitten
destroy kitten
swap chick chick_1
swap tree tree_1
"Kitten: Too bad! See you later."
anykey
desc

'*'
notzero FLAG_YESNO
at room8
present seguin_upseting
"Seguin: Get out of my vegetable garden, then!"
done

'*'
notzero FLAG_YESNO
at room18
present hole_1
done

'*'
at room6
notzero FLAG_CORDE
"I can't, since the rope is holding me back."
done

'*'
at room6
goto room9
desc

'*'
at room7
present door_opened
goto room10
desc

'*'
at room7
"I can't, since the door is closed."
done

'*'
at room16
present door_opened
place door_opened room19
goto room19
desc

'*'
at room16
"I can't, since the door is closed."
done

'*'
at room18
goto room17
desc

'*'
at room22
present boulder_1
"I can't, since the boulder is in the way."
done

'*'
at room22
goto room21
desc

'*'
at room28
present boulder_1
"I can't, since the boulder is in the way."
done

'*'
at room28
goto room21
desc

'*'
at room27
zero FLAG_PATH
set FLAG_PATH
swap track chamois
place boulder room24
swap sign sign_broken
create bone
"As Blanquette begins to ascend the mountain, a huge boulder comes loose, crushing everything in its path."
anykey
desc

'*'
at room27
"The path is too dangerous to ascend it without an experienced guide."
done

@s
'*'
at room6
eq FLAG_CORDE 255
"I can't, since the rope is holding me back."
done

'*'
at room6
goto room3
desc

'*'
at room8
notzero FLAG_CORDE
"I can't, since the rope is holding me back."
done

'*'
at room8
goto room5
desc

'*'
at room12
"I can't, since the door is locked."
done

'*'
at room19
present door_opened
place door_opened room16
goto room16
desc

'*'
at room19
"I can't, since the door is closed."
done

'*'
at room21
notzero FLAG_BOULDER
"I can't, since the boulder is in the way."
done

'*'
at room21
goto room28
desc

'*'
atgt room28
"You can't run away! You have to fight."
done

@e
'*'
at room6
eq FLAG_CORDE 1
"I can't, since the rope is holding me back."
done

'*'
at room6
goto room7
desc

'*'
at room7
"I can't, since the fence is in the way."
done

'*'
present barrel_1
set FLAG_BARREL
done

'*'
at room12
"I can't reach the window from here."
done

'*'
present elves
"Elf: Hey! Blanquette, where are you going? Comes to dance with us./n"
"Blanquette is dragged into the farandole, and comes out dazed."
done

'*'
at room14
"I can't, since part of the bridge has been washed away by the river."
done

'*'
at room18
zero FLAG_SHORTCUT
set FLAG_SHORTCUT
set FLAG_SCORE_PLUS
"I found a shortcut through a winding path covered in brush!"
anykey

'*'
at room18
goto room15
desc

'*'
at room21
set FLAG_DARK
goto room20
desc

'*'
at room24
zero FLAG_SHORTCUT
set FLAG_SHORTCUT
set FLAG_SCORE_PLUS
"I found a shortcut through a narrow, low passage."
anykey

'*'
at room24
goto room21
desc

@w
'*'
at room4
notzero FLAG_CORDE
let FLAG_CORDE 1

'*'
at room4
goto room3
desc

'*'
at room8
"I can't, since the fence is in the way."
done

'*'
at room15
zero FLAG_SHORTCUT
set FLAG_SHORTCUT
set FLAG_SCORE_PLUS
"I found a shortcut through a winding path covered in brush!"
anykey

'*'
at room15
goto room18
desc

'*'
at room20
clear FLAG_DARK
goto room21
desc

'*'
at room21
zero FLAG_SHORTCUT
set FLAG_SHORTCUT
set FLAG_SCORE_PLUS
"I found a shortcut through a narrow, low passage."
anykey

'*'
at room21
goto room24
desc

'*'
at room24
"I can't, since part of the bridge has been washed away by the river."
done

@u
'barrel'
present kitten_asleep
"I must wake Kitten first."
done

'barrel'
present barrel
"Blanquette jumps on the barrel, peers out of the window on the opposite wall, then climbs back down."
done

'barrel'
present barrel_1
set FLAG_BARREL
done

'window'
present barrel_1
set FLAG_BARREL
done

'window'
at room12
"I can't reach the window from here."
done

'river'
at room14
"I don't jump as far as the springbok."
done

'river'
at room24
"I don't jump as far as the springbok."
done

'*'
at room7
goto room8
desc

'*'
at room8
goto room7
desc

'*'
at room2
notzero FLAG_CORDE
"I can't, since the rope is holding me back."
done

'*'
at room2
goto room11
desc

'*'
"Blanquette gives a kick."
done

@enter
'hole'
present cart
"I can't, since the hole is under the overturned cart."
done

'hole'
present hole
"I can't, since the hole is too narrow."
done

'hole'
present hole_1
"Underground voice: Did you do everything you intended to do around here? (Y/N)"
Let FLAG_YESNO 2
done

@run
'*'
atgt room28
"You can't run away! You have to fight."
done

@x
'key'
present salamander
"The key is at the bottom of the basin, among the dead leaves."
done

'key'
present key
"A large antique key weakened by rust."
done

'key'
present key_broken
"All that's left is to make an identical key."
done

'tambourine'
present tambourine
"A circular percussion instrument with pairs of small metal jingles."
done

'dung'
present dung
"A high-quality flytrap."
done

'dung'
present dung_fly
"There's a lot of buzz around it!"
done

'bag'
present owl
"The burlap sack is used as a blanket by the owl."
done

'bag'
present bag_empty
"The bag is empty, and just needs to be filled."
done

'bag'
present bag_flour
"The bag is full, and ready to go."
done

'torch'
present torch_lit
"A flaming torch to see in the dark."
done

'torch'
present torch_unlit
"It's just waiting to be lighted."
done

'book'
present book
"It says: Great Kitchen Dictionary by Alexandre Dumas."
done

'bone'
present bone
"A pierced bone from a goat's leg."
done

'sign'
present sign_broken
"It seems suitable for firewood."
done

'deck'
set FLAG_INV
"The deck contains:\n Basic rules"

'deck'
notzero FLAG_CARD4
" [1] The Kitten card"

'deck'
notzero FLAG_CARD2
" [2] The Toadstool card"

'deck'
notzero FLAG_CARD1
" [3] The Four-leaf Clover card"

'deck'
notzero FLAG_CARD3
" [4] The Venomous Snake card"

'deck'
notzero FLAG_CARD5
" [5] The Morning Star card"

'deck'
done

'card'
present card0
"The card seems cursed, and I have no way of blessing it."
done

'card'
present card1
"With this card added to your deck, the final battle will be in your favour."
done

'card'
present card2
"With this card added to your deck, the final battle will be in your favour."
done

'card'
present card3
"With this card added to your deck, the final battle will be in your favour."
done

'card'
present card4
"With this card added to your deck, the final battle will be in your favour."
done

'card'
present card5
"With this card added to your deck, you're ready for the final battle."
done

'*'
"You can only usefully examine objects surrounded by (*).\nType HELP, please."
done

@read
'rule'
"It says: When Blanquette has to fight her destiny, choose an available card from your deck, and type the number associated with it."
done

'sign'
at room3
zero FLAG_OBEDIENT
set FLAG_OBEDIENT
set FLAG_SCORE_PLUS

'sign'
at room3
"You're obedient, which is good."
done

'sign'
present sign
at room24
"It's says: Risk of falling rocks!"
done

'book'
present book
drop book
get book
"There's a recipe for lamb cooked over a wood fire."
done

'boulder'
present boulder_1
"It says: There must be another entrance somewhere."
done

@get
'card'
present card0
"I'd rather not, as it could have a negative effect on my deck."
done

'card'
present card1
plus FLAG_NCARDS 1
destroy card1
set FLAG_CARD1
desc

'card'
present card2
plus FLAG_NCARDS 1
destroy card2
set FLAG_CARD2
desc

'card'
present card3
plus FLAG_NCARDS 1
destroy card3
set FLAG_CARD3
desc

'card'
present card4
plus FLAG_NCARDS 1
destroy card4
set FLAG_CARD4
desc

'card'
present card5
present torch_lit
create torch_lit
destroy card5
set FLAG_CARD5
desc

'key'
at room13
present salamander
"I can't reach it from here."
"\nSalamander: I'll get it for you if you bring me some flies to nibble on."
done

'key'
present key_broken
notcarr key_broken
"I can leave it there, since I don't need it anymore."
done

'key'
present key
get key
desc

'key'
get key_broken
desc

'tambourine'
present elves
"Elf: You can have it when the party's over."
done

'tambourine'
get tambourine
desc

'dung'
at room13
absent salamander
"I can leave it there, since I don't need it anymore."
done

'dung'
present dung
get dung
desc

'dung'
get dung_fly
desc

'bag'
present owl
"I can't reach it from here."
done

'bag'
present bag_empty
get bag_empty
desc

'bag'
get bag_flour
desc

'torch'
present torch_unlit
present hermit
"Hermit: Hey! Nobody, you know well that I hate anybody touching my stuff."
done

'torch'
at room20
present torch_lit
"I can leave it there, since I don't need it anymore."
done

'torch'
present torch_unlit
get torch_unlit
desc

'torch'
get torch_lit
desc

'book'
get book
desc

'bone'
notzero FLAG_BOULDER
present bone
notcarr bone
"I can leave it there, since I don't need it anymore."
done

'bone'
get bone
desc

'sign'
present sign_broken
present boulder
"I can't, since the sign is stuck under the boulder."
done

'sign'
present sign_broken
get sign_broken
desc

'*'
"You can only get objects surrounded by (*).\nType HELP, please."
done

@drop
'deck'
"You can't, since Blanquette need it for the final fight."
done

'key'
present key
drop key
desc

'key'
drop key_broken
desc

'tambourine'
drop tambourine
desc

'dung'
carried dung
present mule_fly
swap mule_fly mule
destroy dung
create dung_fly
desc

'dung'
present dung
drop dung
desc

'dung'
drop dung_fly
desc

'bag'
present bag_empty
drop bag_empty
desc

'bag'
drop bag_flour
desc

'torch'
at room20
present torch_unlit
drop torch_unlit
place torch_unlit room22
ok

'torch'
at room28
present torch_unlit
drop torch_unlit
place torch_unlit room22
ok

'torch'
present torch_unlit
drop torch_unlit
desc

'torch'
at room28
drop torch_lit
place torch_lit room22
ok

'torch'
drop torch_lit
desc

'book'
at room20
drop book
place book room22
ok

'book'
at room28
drop book
place book room22
ok

'book'
drop book
desc

'bone'
at room20
drop bone
place bone room22
ok

'bone'
at room28
drop bone
place bone room22
ok

'bone'
drop bone
desc

'sign'
present sign_broken
at room20
drop sign_broken
place sign_broken room22
ok

'sign'
present sign_broken
drop sign_broken
desc

'sign'
drop sign
desc

@fill
'bag'
at room19
present bag_flour
"The bag is already full of flour."
done

'bag'
at room19
present bag_empty
drop bag_empty
get bag_empty
swap bag_empty bag_flour
"You fill the bag with the flour from the millstone."
done

'bag'
notat room19
present bag_empty
drop bag_empty
get bag_empty
"There isn't anything obvious with which to fill it."
done

@empty
'bag'
present bag_empty
"The bag is already empty."
done

'bag'
present bag_flour
drop bag_flour
get bag_flour
swap bag_flour bag_empty
ok

@wear
'tambourine'
drop tambourine
get tambourine
"I'd rather not. I'll never again be restrained by a collar and leash, rope or chain."
done

@give
'deck'
"You can't, since Blanquette need it for the final fight."
done

'dung'
present dung_fly
present salamander
drop dung_fly
get dung_fly
destroy salamander
destroy dung_fly
create dung
"The flies rush to the salamander, which swallows them at once. The salamander vanishes under the leaves after having brought back the key."
anykey
desc

'tambourine'
present mule
drop tambourine
get tambourine
set FLAG_SNAKE
swap snake card3
destroy mule
destroy cart
destroy tambourine
place mule_cart room16
"Mule: Thanks! This will make a pretty bell collar. See you later."
anykey
desc

'tambourine'
present snake
drop tambourine
get tambourine
"Snake: I'm not a pet to be tied up or caged!"
done

'bag'
present bag_empty
present mule_cart
drop bag_empty
get bag_empty
"But the bag is empty at the moment."
done

'bag'
present mule_cart
drop bag_flour
get bag_flour
"Mule: PUT it in the cart."
done

'book'
present hermit
drop book
plus FLAG_CAMPFIRE 1
destroy book
"Hermit: Thanks! Nobody. And what else?"
done

'sign'
present hermit
drop sign_broken
plus FLAG_CAMPFIRE 1
destroy sign_broken
"Hermit: Thanks! Nobody. And what else?"
done

'*'
"No one seems interested."
done

@put
'bag'
present bag_empty
drop bag_empty
get bag_empty
Let FLAG_PUTIN 2
"In what?"
done

'bag'
drop bag_flour
get bag_flour
Let FLAG_PUTIN 2
"In what?"
done

@cart
'*'
notzero FLAG_PUTIN
carried bag_empty
present mule_cart
"But the bag is empty at the moment."
done

'*'
notzero FLAG_PUTIN
present mule_cart
destroy bag_flour
destroy mule_cart
set FLAG_SCORE_PLUS
"Mule: Thanks! See you later."
anykey
desc

@talk
'kitten'
at room11
present kitten
Let FLAG_YESNO 2
"Kitten: Hi Blanquette, I can't see my beloved sister from up here. Do you know where she's hidden? (Y/N)"
done

'seguin'
present seguin_milking
clear FLAG_CORDE
destroy rope
destroy seguin_milking
"Seguin: Congratulations Blanquette, your milk is of very good quality. Now that my bucket is full, I'm going to take a little nap."
anykey
desc

'seguin'
present seguin_upseting
zero FLAG_SEGUIN
set FLAG_SEGUIN
"Seguin: A rabbit stole my carrots again! Hey! Blanquette, what's going on?\n\n*: I want to go to the mountain, Mr Seguin.\n\nSeguin: But don't you know that there's a wolf in the mountain? What will you do when he comes?\n\n*: It doesn't matter, Mr Seguin, let me go to the mountain.\n\nSeguin: I will lock you in the stable, and you will stay there!\n"

'seguin'
present seguin_upseting
"Seguin: Did you do everything you intended to do around here? (Y/N)"
Let FLAG_YESNO 2
done

'rooster'
present rooster
swap rooster rooster_1
place seguin_upseting room8
"Rooster: Hi Blanquette, the wild freedom is on the other side of the wall. LISTEN to the mountain breath, and you will know the great price of freedom."
"\n*: How good it must be up there! Without fences or rope that skin my neck."
"\nRooster: By the way, I saw Mr Seguin in the vegetable garden."
anykey
desc

'rooster'
present rooster_1
"Rooster: COCK-A-DOODLE-DO!"
done

'chick'
present chick
"Chick: Hi Blanquette, if you see my brother Kitten, don't tell him where I'm hiding!"
done

'chick'
present chick_1
"Chick: CHEEP-CHEEP!"
done

'salamander'
present salamander
"Salamander: Hi Blanquette, you shouldn't be here, Mr Seguin will be worried."
done

'elves'
present elves
"Elf: Hi Blanquette, we're celebrating the Shepherd's day, come to dance with us./n"
"Blanquette is dragged into the farandole, and comes out dazed."
done

'mule'
present mule_fly
"Mule: Hi Blanquette, I have to fetch the wrecked hay wagon, but a nasty snake is in my way. Oh, if only I had my bell collar to scare him off!"
done

'mule'
present mule
"Mule: Hi Blanquette, I have to fetch the wrecked hay wagon, but a nasty snake is in my way. Oh, if only I had my bell collar to scare him off!"
done

'mule'
present mule_cart
"Mule: Hi Blanquette, I have to bring a bag of flour to Mr Seguin. If you have one, PUT it in the cart."
done

'snake'
present snake
"Snake: Hi blanquette, the mule, far too clumsy to take the shortcut, nearly ran me over as I passed. Next time, I'll bite her!"
done

'owl'
present owl
"Owl: Hi blanquette, I see little rabbits grazing on the clover outside the windmill, but I'm too old to chase them."
done

'rabbit'
present rabbit_leisurely
present door_opened
destroy rabbit_leisurely
place rabbit_fearful room19
set FLAG_RABBIT
"The scared rabbit runs inside windmill at high speed."
anykey
desc

'rabbit'
present rabbit_leisurely
destroy rabbit_leisurely
"The scared rabbit runs away at high speed."
anykey
desc

'rabbit'
present rabbit_fearful
present door_opened
destroy rabbit_fearful
clear FLAG_RABBIT
"The scared rabbit runs away at high speed."
anykey
desc

'rabbit'
present rabbit_fearful
destroy rabbit_fearful
destroy owl
place card1 room16
let FLAG_STORM 1
"The frightened rabbit runs away at high speed, and hits the closed door.\n\nOwl: Hey! what's going on?\n\nThe owl dives on the stunned rabbit, and carries its prey to safety.\n\nThe burlap bag fell off the roof framework."
anykey
desc

'hermit'
at room21
present hermit
zero FLAG_NOBODY
set FLAG_NOBODY
"Hermit: Hi Nobody, bring me some stale bread, wood and paper so I can make a campfire toast."
"\n*: I'm not Nobody, I'm Blanquette!\n\nHermit: Ha ha ha! And I'm Polypheme! What a joker this Nobody."
done

'hermit'
at room21
present hermit
"Hermit: Hi Nobody, bring me some stale bread, wood and paper so I can make a campfire toast."
done

'ant'
present ant
"Ant: Hi Blanquette, the hermit and his lamb trample us every time they come out of the cave. If you find anything to block the entrance, let me know where by blowing a loud whistle."
done

'lamb'
present lamb
zero FLAG_LAMB
set FLAG_LAMB
set FLAG_SCORE_PLUS

'lamb'
present lamb
"Robin: Hi Blanquette, I'm sure the hermit wants to eat me to atone for his sins!"
done

'lamb'
present lamb_1
"Lamb: BAA MAA!"
done

'chamois'
present chamois
notzero FLAG_CARD5
dropall
get deck
clear FLAG_CARD5
goto room29
"Chamois: It's time to face your destiny, Blanquette.\n\nThe deck of cards shakes and lights up, the Morning Star card springs up to join the firmament. It's pitch-dark now."
anykey
desc

'chamois'
present chamois
"Chamois: Hi Blanquette, find the Morning Star card, and I'll lead you on the path to your destiny.\nDon't forget to SAVE before the final battle."
done

'moth'
notzero FLAG_BOULDER
at room20
absent torch_lit
"Moth: ..."
done

'moth'
at room20
absent torch_lit
"Moth: Hi Blanquette, I saw a lamb runs away at high speed to the west, south and west again."
done

'*'
"There's no reply."
done

@wake
'kitten'
present kitten_asleep
notzero FLAG_LICK
swap kitten_asleep barrel
"Kitten stretches, licks his paw, and disgusted by the taste of salt, runs away through the window."
anykey
desc

'kitten'
present kitten_asleep
"Kitten stretches, licks his paw to clean his hair, and falls back to sleep."
done

'hermit'
present hermit_1
"Nothing can wake him up."
done

@eat
'daisy'
present daisy
destroy daisy
minus FLAG_EAT 1
set FLAG_SCORE_PLUS
desc

'carrot'
present carrot
destroy carrot
minus FLAG_EAT 1
set FLAG_SCORE_PLUS
desc

'almond'
present almond
destroy almond
minus FLAG_EAT 1
set FLAG_SCORE_PLUS
desc

'salt'
present salt_stone
"It's not for eating, it's for licking."
done

'*'
"It's not my favourite meal."
done

@lick
'salt'
present salt_stone
set FLAG_SALT
"My tongue is now full of salt."
done

'kitten'
present kitten_asleep
notzero FLAG_SALT
set FLAG_LICK

'kitten'
present kitten_asleep
"Kitten purrs with pleasure in his sleep."
done

@drink
'*'
at room14
"Blanquette quenches its thirst, and splashes with fresh water."
done

'*'
at room24
"Blanquette quenches its thirst, and splashes with fresh water."
done

'*'
at room23
present lamb
"The lamb seems hypnotized by the water's reflection of Blanquette.\n\nRobin: Hey! Blanquette, what big eyes you have! What big ears you have! What big teeth you have!\n\nThe scared lamb runs away at high speed."
destroy lamb
place lamb_1 room21
plus FLAG_CAMPFIRE 1
anykey
desc

'*'
at room23
"Blanquette quenches its thirst, and splashes with fresh water."
done

'*'
"There's nothing suitable to drink here."
done

@open
'door'
present door_opened
"The door is already open."
done

'door'
present door_locked
"I can't, since the door is locked."
done

'door'
present door_closed
swap door_closed door_opened
desc

@close
'door'
present door_closed
"The door is already closed."
done

'door'
present door_locked
"The door is already closed."
done

'door'
at room7
present door_opened
"Seguin: Hey! Blanquette, leave this door open."
done

'door'
present door_opened
swap door_opened door_closed
desc

@lock
'door'
present door_locked
"The door is already locked."
done

'door'
present door_opened
"I can't, since the door is opened."
done

'door'
present door_closed
notcarr key
"I need a key."
done

'door'
present door_closed
swap door_closed door_locked
desc

@unlock
'door'
present door_locked
notcarr key
"I need a key."
done

'door'
at room16
present door_locked
swap door_locked door_closed
swap key key_broken
desc

@rub
'card'
present card0
swap card0 card4
desc

'*'
"I achieve nothing by this."
done

@burn
'torch'
present torch_unlit
present campfire
drop torch_unlit
get torch_unlit
swap torch_unlit torch_lit
place hermit_1 room21
swap campfire ashes
desc

'torch'
present torch_unlit
"There's nothing here to light it."
done

'skeleton'
present skeleton
"Renaude didn't want to be cremated."
done

'*'
"This dangerous act would achieve little."
done

@extinguish
'torch'
present torch_lit
"The flame dies, and is reborn again."
done

'campfire'
present campfire
"The flame dies, and is reborn again."
done

@break
'rope'
present rope
"I can't, since the rope is strong and the knot very tight."
done

'*'
"I could get hurt."
done

@attack
'*'
atgt room29
"Use your cards to fight."
done

'*'
"Violence isn't the answer to this one."
done

@move
'cart'
present cart
"I don't have enough strength to lift it."
done

'statue'
present statue
"I don't have enough strength to move it."
done

'barrel'
present kitten_asleep
"I must wake Kitten first."
done

'barrel'
present barrel
absent barrel_1
swap barrel barrel_1
"The barrel rolls under the window."
anykey
desc

'kitten'
present kitten_asleep
"Violence isn't the answer to this one."
done

'boulder'
present boulder
"I don't have enough strength to move it."
done

'boulder'
present boulder_1
"I don't have enough strength to move it."
done

'boulder'
at room21
notzero FLAG_BOULDER
"I don't have enough strength to move it."
done

'skeleton'
present skeleton
"Leave her in peace. The vultures will take care of it."
done

@play
'tambourine'
at room17
present snake
carried tambourine
zero FLAG_JINGLE
set FLAG_SCORE_PLUS
set FLAG_JINGLE

'tambourine'
at room17
present snake
drop tambourine
get tambourine
destroy snake
"~TING-A-LING!~\n\nAt the sound of the jingle, the snake runs away at high speed."
anykey
desc

'tambourine'
drop tambourine
get tambourine
"~TING-A-LING!~"
done

'bone'
present bone
drop bone
get bone
"Try to BLOW into it instead."
done

@num1
'*'
atgt room29
notzero FLAG_CARD4
clear FLAG_CARD4
let FLAG_PLAY 1
minus FLAG_NCARDS 1
" [1] The Kitten card"
done

'*'
atgt room29
zero FLAG_CARD4
"This card is not in your deck."
done

@num2
'*'
atgt room29
notzero FLAG_CARD2
clear FLAG_CARD2
let FLAG_PLAY 2
minus FLAG_NCARDS 1
" [2] The Toadstool card"
done

'*'
atgt room29
zero FLAG_CARD2
"This card is not in your deck."
done

@num3
'*'
atgt room29
notzero FLAG_CARD1
clear FLAG_CARD1
let FLAG_PLAY 3
minus FLAG_NCARDS 1
" [3] The Four-leaf Clover card"
done

'*'
atgt room29
zero FLAG_CARD1
"This card is not in your deck."
done

@num4
'*'
atgt room29
notzero FLAG_CARD3
clear FLAG_CARD3
let FLAG_PLAY 4
minus FLAG_NCARDS 1
" [4] The Venomous Snake card"
done

'*'
atgt room29
zero FLAG_CARD3
"This card is not in your deck."
done

@blow
'bone'
at room24
present boulder
drop bone
get bone
destroy boulder
swap ant boulder_1
set FLAG_BOULDER
"~TWEET TWEET TWEET!~"
"\nThe ground darkened and began to move, dragging the rock with it.\n\nAnt: Thanks! See you later."
anykey
desc

'bone'
present ant
drop bone
get bone
"~TWEET TWEET TWEET!~"
"\nAnt: Blow a loud whistle where you find anything to block the entrance."
done

'bone'
drop bone
get bone
"~TWEET TWEET TWEET!~"
done

'*'
"~TWEET!~"
done

@dig
'*'
present hole
absent cart
swap hole hole_1
desc

'*'
present skeleton
"Renaude didn't want to be buried."
done

'*'
"Digging would achieve nothing here."
done

@dance
'*'
present elves
"Blanquette is dragged into the farandole, and comes out dazed."
done

'*'
"Blanquette move in a graceful and rhythmical way."
done

@swim
'*'
at room14
"The river's current is too strong."
done

'*'
at room24
"The river's current is too strong."
done

@listen
'dung'
present dung_fly
"There's a lot of buzz around it!"
done

'*'
at room9
zero FLAG_LISTEN
set FLAG_LISTEN
set FLAG_SCORE_PLUS

'*'
at room9
"I hear a wolf howling in the distance."
done

'*'
present hole
"Underground voice: BAA MAA!"
done

'*'
present hole_1
"~BAA MAA!~"
done

'*'
"I hear nothing unexpected."
done

@smell
'dung'
present dung
"It doesn't smell like roses."
done

'dung'
present dung_fly
"It doesn't smell like roses."
done

'*'
at room6
"A subtle flowery fragrance."
done

'*'
present campfire
"Robin's body odour."
done

'*'
"I smell nothing unexpected."
done

@sleep
'*'
"I'm not feeling especially drowsy."
done

@wait
'*'
"Nothing happens."
done

@ask
'*'
"Use TALK to communicate with characters."
done

@use
'*'
"You don't need to use this verb in this game."
done

@yes
'*'
notzero FLAG_YESNO
at room11
present kitten
destroy kitten
swap card0 chick
swap tree tree_1
"Kitten: Thanks! See you later."
anykey
desc

'*'
notzero FLAG_YESNO
at room8
present seguin_upseting
place door_locked room12
place salt_stone room12
clear FLAG_LICK
goto room12
desc

'*'
notzero FLAG_YESNO
at room18
dropall
get deck
goto room20
set FLAG_DARK
clear FLAG_SHORTCUT
clear FLAG_BOULDER
clear FLAG_PATH
clear FLAG_NOBODY
clear FLAG_CAMPFIRE
"Blanquette is sucked down the rabbit hole, and falls back into a cave, onto a lamb that cushions her fall."
anykey
desc

@help
'*'
cls
"The game expect commands in the form of VERB NOUN (not case-sensitive).\n\nBasic commands:\nL or R to refresh the location display.\nN, S, E, W, U & D to move around.\nI to display what you're carrying and wearing.\nX to examine an object.\nGET & DROP to pick up or drop an object.\nTALK to communicate with a character.\nVERBS to display commands needed to complete the game.\nSAVE & RESTORE to save or restore the game.\n\nInteract only with objects displayed after: I notice.\nEXAMINE (X) and GET are only useful for objects surrounded by (*) e.g. A *key*."
anykey
desc

@about
'*'
"Mr Seguin's Goat (c) 2023 Lionel Ange.\nBased on a short story in Letters from My Windmill (1869), by the French writer and playwright Alphonse Daudet.\nPlaytesting by Nobody and Polypheme.\n\nRelease 1.2 / Serial number 052424 / cQuill\nThe game code is compatible with The Quill (Gilsoft).\n\nThis game was created for Text Adventure Literacy Jam in May 2023."
done

@verbs
'*'
"Some useful commands:\nL (look), I (inventory)\nX (examine), READ\nN, S, E, W, U, D, CLIMB, JUMP, GO\nTALK, GIVE, WAKE, EAT, DRINK, LICK, BLOW\nGET, DROP, PUT, WEAR, REMOVE, FILL, EMPTY\nOPEN, CLOSE, UNLOCK, MOVE, DIG\nLIGHT, EXTINGUISH, CLEAN, PLAY, LISTEN\nSAVE, RESTORE, YES, NO, SCORE\nHELP, ABOUT, VERBS\n\nAnd other commands (or synonyms) that you will discover by yourself."
done

@i
'*'
zero FLAG_INV
Let FLAG_INV 1

'*'
inven

@l
'*'
desc

@score
'*'
score
turns

@q
'*'
quit
turns
end

@save
'*'
atgt room28
"It's too late to save. Blanquette must face her destiny."
done

'*'
save

@load
'*'
load

