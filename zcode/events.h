Constant FLAG_DARK        0;
Constant FLAG_YESNO       5;
Constant FLAG_PUTIN       6;

Constant FLAG_START       11;
Constant FLAG_CORDE       12;
Constant FLAG_EAT         13;
Constant FLAG_OBEDIENT    14;
Constant FLAG_SEGUIN      15;
Constant FLAG_LISTEN      16;

Constant FLAG_PLAY        19;
Constant FLAG_TURN        20;
Constant FLAG_MAX         21;
Constant FLAG_INV         22;
Constant FLAG_SCORE_PLUS  23;
Constant FLAG_NCARDS      24;
Constant FLAG_CARD1       25;
Constant FLAG_CARD2       26;
Constant FLAG_CARD3       27;
Constant FLAG_CARD4       28;
Constant FLAG_CARD5       29;
Constant FLAG_SCORE       30;

Constant FLAG_BARREL      12;
Constant FLAG_SALT        13;
Constant FLAG_LICK        14;

Constant FLAG_SHORTCUT    12;
Constant FLAG_RABBIT      13;
Constant FLAG_SNAKE       14;
Constant FLAG_JINGLE      15;
Constant FLAG_STORM       16;

Constant FLAG_BOULDER     13;
Constant FLAG_PATH        14;
Constant FLAG_NOBODY      15;
Constant FLAG_CAMPFIRE    16;
Constant FLAG_LAMB        17;

Array StatusTable table
    14
    ',_' ',_'
    at_ room0
    set_ FLAG_CORDE
    let_ FLAG_EAT 4
    anykey_
    goto_ room4
    desc_

    16
    ',_' ',_'
    at_ room4
    zero_ FLAG_START
    let_ FLAG_START 1
    destroy_ seguin
    message_ 0
    anykey_
    desc_

    12
    ',_' ',_'
    at_ room4
    eq_ FLAG_START 1
    set_ FLAG_START
    message_ 1

    9
    ',_' ',_'
    at_ room4
    notzero_ FLAG_CORDE
    set_ FLAG_CORDE

    13
    ',_' ',_'
    at_ room10
    notzero_ FLAG_CORDE
    message_ 2
    anykey_
    goto_ room7
    desc_

    14
    ',_' ',_'
    eq_ FLAG_EAT 1
    clear_ FLAG_EAT
    place_ seguin_milking room4
    swap_ door_opened door_locked

    26
    ',_' ',_'
    at_ room12
    notzero_ FLAG_BARREL
    clear_ FLAG_SHORTCUT
    clear_ FLAG_RABBIT
    clear_ FLAG_SNAKE
    clear_ FLAG_JINGLE
    clear_ FLAG_LISTEN
    place_ door_locked room16
    message_ 3
    anykey_
    goto_ room13
    desc_

    10
    ',_' ',_'
    at_ room16
    zero_ FLAG_SNAKE
    place_ snake room17

    10
    ',_' ',_'
    at_ room18
    zero_ FLAG_SNAKE
    place_ snake room17

    10
    ',_' ',_'
    at_ room18
    zero_ FLAG_RABBIT
    place_ rabbit_leisurely room16

    18
    ',_' ',_'
    eq_ FLAG_STORM 1
    at_ room16
    set_ FLAG_STORM
    swap_ elves statue
    cls_
    message_ 4
    anykey_
    desc_

    21
    ',_' ',_'
    at_ room21
    eq_ FLAG_CAMPFIRE 3
    clear_ FLAG_CAMPFIRE
    destroy_ hermit
    swap_ ashes campfire
    destroy_ lamb_1
    message_ 5
    anykey_
    desc_

    10
    ',_' ',_'
    at_ room29
    eq_ FLAG_NCARDS 4
    set_ FLAG_MAX

    11
    ',_' ',_'
    at_ room29
    goto_ room30
    set_ FLAG_TURN
    anykey_
    desc_

    11
    ',_' ',_'
    atgt_ room29
    notzero_ FLAG_TURN
    clear_ FLAG_TURN
    message_ 6

    12
    ',_' ',_'
    at_ room30
    eq_ FLAG_PLAY 1
    message_ 7
    message_ 8

    12
    ',_' ',_'
    at_ room30
    eq_ FLAG_PLAY 2
    message_ 9
    message_ 10

    12
    ',_' ',_'
    at_ room30
    eq_ FLAG_PLAY 3
    message_ 11
    message_ 10

    12
    ',_' ',_'
    at_ room30
    eq_ FLAG_PLAY 4
    message_ 12
    message_ 13

    15
    ',_' ',_'
    at_ room30
    notzero_ FLAG_PLAY
    clear_ FLAG_PLAY
    set_ FLAG_TURN
    goto_ room31
    anykey_
    desc_

    12
    ',_' ',_'
    at_ room31
    eq_ FLAG_PLAY 1
    message_ 14
    message_ 13

    12
    ',_' ',_'
    at_ room31
    eq_ FLAG_PLAY 2
    message_ 15
    message_ 8

    12
    ',_' ',_'
    at_ room31
    eq_ FLAG_PLAY 3
    message_ 11
    message_ 10

    12
    ',_' ',_'
    at_ room31
    eq_ FLAG_PLAY 4
    message_ 16
    message_ 17

    15
    ',_' ',_'
    at_ room31
    notzero_ FLAG_PLAY
    clear_ FLAG_PLAY
    set_ FLAG_TURN
    goto_ room32
    anykey_
    desc_

    12
    ',_' ',_'
    at_ room32
    eq_ FLAG_PLAY 1
    message_ 18
    message_ 8

    12
    ',_' ',_'
    at_ room32
    eq_ FLAG_PLAY 2
    message_ 18
    message_ 8

    12
    ',_' ',_'
    at_ room32
    eq_ FLAG_PLAY 3
    message_ 18
    message_ 8

    12
    ',_' ',_'
    at_ room32
    eq_ FLAG_PLAY 4
    message_ 18
    message_ 8

    15
    ',_' ',_'
    at_ room32
    notzero_ FLAG_PLAY
    clear_ FLAG_PLAY
    set_ FLAG_TURN
    goto_ room33
    anykey_
    desc_

    12
    ',_' ',_'
    at_ room33
    eq_ FLAG_PLAY 1
    message_ 19
    message_ 10

    12
    ',_' ',_'
    at_ room33
    eq_ FLAG_PLAY 2
    message_ 20
    message_ 13

    12
    ',_' ',_'
    at_ room33
    eq_ FLAG_PLAY 3
    message_ 11
    message_ 10

    12
    ',_' ',_'
    at_ room33
    eq_ FLAG_PLAY 4
    message_ 21
    message_ 8

    11
    ',_' ',_'
    at_ room33
    notzero_ FLAG_PLAY
    goto_ room34
    anykey_
    desc_

    12
    ',_' ',_'
    atgt_ room29
    zero_ FLAG_NCARDS
    set_ FLAG_NCARDS
    goto_ room34
    desc_

    11
    ',_' ',_'
    at_ room34
    notzero_ FLAG_MAX
    goto_ room35
    anykey_
    desc_

    9
    ',_' ',_'
    at_ room34
    goto_ room36
    anykey_
    desc_

    8
    ',_' ',_'
    at_ room35
    score_
    turns_
    end_

    8
    ',_' ',_'
    at_ room36
    score_
    turns_
    end_

    12
    ',_' ',_'
    notzero_ FLAG_SCORE_PLUS
    clear_ FLAG_SCORE_PLUS
    message_ 22
    plus_ FLAG_SCORE 10

    10
    ',_' ',_'
    eq_ FLAG_INV 1
    set_ FLAG_INV
    message_ 23
    0
;

Array EventTable table
    21
    'n//' ',_'
    notzero_ FLAG_YESNO
    at_ room11
    present_ kitten
    destroy_ kitten
    swap_ chick chick_1
    swap_ tree tree_1
    message_ 24
    anykey_
    desc_

    12
    'n//' ',_'
    notzero_ FLAG_YESNO
    at_ room8
    present_ seguin_upseting
    message_ 25
    done_

    10
    'n//' ',_'
    notzero_ FLAG_YESNO
    at_ room18
    present_ hole_1
    done_

    10
    'n//' ',_'
    at_ room6
    notzero_ FLAG_CORDE
    message_ 26
    done_

    8
    'n//' ',_'
    at_ room6
    goto_ room9
    desc_

    10
    'n//' ',_'
    at_ room7
    present_ door_opened
    goto_ room10
    desc_

    8
    'n//' ',_'
    at_ room7
    message_ 27
    done_

    13
    'n//' ',_'
    at_ room16
    present_ door_opened
    place_ door_opened room19
    goto_ room19
    desc_

    8
    'n//' ',_'
    at_ room16
    message_ 27
    done_

    8
    'n//' ',_'
    at_ room18
    goto_ room17
    desc_

    10
    'n//' ',_'
    at_ room22
    present_ boulder_1
    message_ 28
    done_

    8
    'n//' ',_'
    at_ room22
    goto_ room21
    desc_

    10
    'n//' ',_'
    at_ room28
    present_ boulder_1
    message_ 28
    done_

    8
    'n//' ',_'
    at_ room28
    goto_ room21
    desc_

    24
    'n//' ',_'
    at_ room27
    zero_ FLAG_PATH
    set_ FLAG_PATH
    swap_ track chamois
    place_ boulder room24
    swap_ sign sign_broken
    create_ bone
    message_ 29
    anykey_
    desc_

    8
    'n//' ',_'
    at_ room27
    message_ 30
    done_

    11
    's//' ',_'
    at_ room6
    eq_ FLAG_CORDE 255
    message_ 26
    done_

    8
    's//' ',_'
    at_ room6
    goto_ room3
    desc_

    10
    's//' ',_'
    at_ room8
    notzero_ FLAG_CORDE
    message_ 26
    done_

    8
    's//' ',_'
    at_ room8
    goto_ room5
    desc_

    8
    's//' ',_'
    at_ room12
    message_ 31
    done_

    13
    's//' ',_'
    at_ room19
    present_ door_opened
    place_ door_opened room16
    goto_ room16
    desc_

    8
    's//' ',_'
    at_ room19
    message_ 27
    done_

    10
    's//' ',_'
    at_ room21
    notzero_ FLAG_BOULDER
    message_ 28
    done_

    8
    's//' ',_'
    at_ room21
    goto_ room28
    desc_

    8
    's//' ',_'
    atgt_ room28
    message_ 32
    done_

    11
    'e//' ',_'
    at_ room6
    eq_ FLAG_CORDE 1
    message_ 26
    done_

    8
    'e//' ',_'
    at_ room6
    goto_ room7
    desc_

    8
    'e//' ',_'
    at_ room7
    message_ 33
    done_

    8
    'e//' ',_'
    present_ barrel_1
    set_ FLAG_BARREL
    done_

    8
    'e//' ',_'
    at_ room12
    message_ 34
    done_

    10
    'e//' ',_'
    present_ elves
    message_ 35
    message_ 36
    done_

    8
    'e//' ',_'
    at_ room14
    message_ 37
    done_

    14
    'e//' ',_'
    at_ room18
    zero_ FLAG_SHORTCUT
    set_ FLAG_SHORTCUT
    set_ FLAG_SCORE_PLUS
    message_ 38
    anykey_

    8
    'e//' ',_'
    at_ room18
    goto_ room15
    desc_

    10
    'e//' ',_'
    at_ room21
    set_ FLAG_DARK
    goto_ room20
    desc_

    14
    'e//' ',_'
    at_ room24
    zero_ FLAG_SHORTCUT
    set_ FLAG_SHORTCUT
    set_ FLAG_SCORE_PLUS
    message_ 39
    anykey_

    8
    'e//' ',_'
    at_ room24
    goto_ room21
    desc_

    10
    'w//' ',_'
    at_ room4
    notzero_ FLAG_CORDE
    let_ FLAG_CORDE 1

    8
    'w//' ',_'
    at_ room4
    goto_ room3
    desc_

    8
    'w//' ',_'
    at_ room8
    message_ 33
    done_

    14
    'w//' ',_'
    at_ room15
    zero_ FLAG_SHORTCUT
    set_ FLAG_SHORTCUT
    set_ FLAG_SCORE_PLUS
    message_ 38
    anykey_

    8
    'w//' ',_'
    at_ room15
    goto_ room18
    desc_

    10
    'w//' ',_'
    at_ room20
    clear_ FLAG_DARK
    goto_ room21
    desc_

    14
    'w//' ',_'
    at_ room21
    zero_ FLAG_SHORTCUT
    set_ FLAG_SHORTCUT
    set_ FLAG_SCORE_PLUS
    message_ 39
    anykey_

    8
    'w//' ',_'
    at_ room21
    goto_ room24
    desc_

    8
    'w//' ',_'
    at_ room24
    message_ 37
    done_

    8
    'u//' 'barrel'
    present_ kitten_asleep
    message_ 40
    done_

    8
    'u//' 'barrel'
    present_ barrel
    message_ 41
    done_

    8
    'u//' 'barrel'
    present_ barrel_1
    set_ FLAG_BARREL
    done_

    8
    'u//' 'window'
    present_ barrel_1
    set_ FLAG_BARREL
    done_

    8
    'u//' 'window'
    at_ room12
    message_ 34
    done_

    8
    'u//' 'river'
    at_ room14
    message_ 42
    done_

    8
    'u//' 'river'
    at_ room24
    message_ 42
    done_

    8
    'u//' ',_'
    at_ room7
    goto_ room8
    desc_

    8
    'u//' ',_'
    at_ room8
    goto_ room7
    desc_

    10
    'u//' ',_'
    at_ room2
    notzero_ FLAG_CORDE
    message_ 26
    done_

    8
    'u//' ',_'
    at_ room2
    goto_ room11
    desc_

    6
    'u//' ',_'
    message_ 43
    done_

    8
    'enter' 'hole'
    present_ cart
    message_ 44
    done_

    8
    'enter' 'hole'
    present_ hole
    message_ 45
    done_

    11
    'enter' 'hole'
    present_ hole_1
    message_ 46
    Let_ FLAG_YESNO 2
    done_

    8
    'run' ',_'
    atgt_ room28
    message_ 32
    done_

    8
    'x//' 'key'
    present_ salamander
    message_ 47
    done_

    8
    'x//' 'key'
    present_ key
    message_ 48
    done_

    8
    'x//' 'key'
    present_ key_broken
    message_ 49
    done_

    8
    'x//' 'tambourine'
    present_ tambourine
    message_ 50
    done_

    8
    'x//' 'dung'
    present_ dung
    message_ 51
    done_

    8
    'x//' 'dung'
    present_ dung_fly
    message_ 52
    done_

    8
    'x//' 'bag'
    present_ owl
    message_ 53
    done_

    8
    'x//' 'bag'
    present_ bag_empty
    message_ 54
    done_

    8
    'x//' 'bag'
    present_ bag_flour
    message_ 55
    done_

    8
    'x//' 'torch'
    present_ torch_lit
    message_ 56
    done_

    8
    'x//' 'torch'
    present_ torch_unlit
    message_ 57
    done_

    8
    'x//' 'book'
    present_ book
    message_ 58
    done_

    8
    'x//' 'bone'
    present_ bone
    message_ 59
    done_

    8
    'x//' 'sign'
    present_ sign_broken
    message_ 60
    done_

    7
    'x//' 'deck'
    set_ FLAG_INV
    message_ 61

    7
    'x//' 'deck'
    notzero_ FLAG_CARD4
    message_ 62

    7
    'x//' 'deck'
    notzero_ FLAG_CARD2
    message_ 63

    7
    'x//' 'deck'
    notzero_ FLAG_CARD1
    message_ 64

    7
    'x//' 'deck'
    notzero_ FLAG_CARD3
    message_ 65

    7
    'x//' 'deck'
    notzero_ FLAG_CARD5
    message_ 66

    4
    'x//' 'deck'
    done_

    8
    'x//' 'card'
    present_ card0
    message_ 67
    done_

    8
    'x//' 'card'
    present_ card1
    message_ 68
    done_

    8
    'x//' 'card'
    present_ card2
    message_ 68
    done_

    8
    'x//' 'card'
    present_ card3
    message_ 68
    done_

    8
    'x//' 'card'
    present_ card4
    message_ 68
    done_

    8
    'x//' 'card'
    present_ card5
    message_ 69
    done_

    6
    'x//' ',_'
    message_ 70
    done_

    6
    'read' 'rule'
    message_ 71
    done_

    11
    'read' 'sign'
    at_ room3
    zero_ FLAG_OBEDIENT
    set_ FLAG_OBEDIENT
    set_ FLAG_SCORE_PLUS

    8
    'read' 'sign'
    at_ room3
    message_ 72
    done_

    10
    'read' 'sign'
    present_ sign
    at_ room24
    message_ 73
    done_

    12
    'read' 'book'
    present_ book
    drop_ book
    get_ book
    message_ 74
    done_

    8
    'read' 'boulder'
    present_ boulder_1
    message_ 75
    done_

    8
    'get' 'card'
    present_ card0
    message_ 76
    done_

    13
    'get' 'card'
    present_ card1
    plus_ FLAG_NCARDS 1
    destroy_ card1
    set_ FLAG_CARD1
    desc_

    13
    'get' 'card'
    present_ card2
    plus_ FLAG_NCARDS 1
    destroy_ card2
    set_ FLAG_CARD2
    desc_

    13
    'get' 'card'
    present_ card3
    plus_ FLAG_NCARDS 1
    destroy_ card3
    set_ FLAG_CARD3
    desc_

    13
    'get' 'card'
    present_ card4
    plus_ FLAG_NCARDS 1
    destroy_ card4
    set_ FLAG_CARD4
    desc_

    14
    'get' 'card'
    present_ card5
    present_ torch_lit
    create_ torch_lit
    destroy_ card5
    set_ FLAG_CARD5
    desc_

    12
    'get' 'key'
    at_ room13
    present_ salamander
    message_ 77
    message_ 78
    done_

    10
    'get' 'key'
    present_ key_broken
    notcarr_ key_broken
    message_ 79
    done_

    8
    'get' 'key'
    present_ key
    get_ key
    desc_

    6
    'get' 'key'
    get_ key_broken
    desc_

    8
    'get' 'tambourine'
    present_ elves
    message_ 80
    done_

    6
    'get' 'tambourine'
    get_ tambourine
    desc_

    10
    'get' 'dung'
    at_ room13
    absent_ salamander
    message_ 79
    done_

    8
    'get' 'dung'
    present_ dung
    get_ dung
    desc_

    6
    'get' 'dung'
    get_ dung_fly
    desc_

    8
    'get' 'bag'
    present_ owl
    message_ 77
    done_

    8
    'get' 'bag'
    present_ bag_empty
    get_ bag_empty
    desc_

    6
    'get' 'bag'
    get_ bag_flour
    desc_

    10
    'get' 'torch'
    present_ torch_unlit
    present_ hermit
    message_ 81
    done_

    10
    'get' 'torch'
    at_ room20
    present_ torch_lit
    message_ 79
    done_

    8
    'get' 'torch'
    present_ torch_unlit
    get_ torch_unlit
    desc_

    6
    'get' 'torch'
    get_ torch_lit
    desc_

    6
    'get' 'book'
    get_ book
    desc_

    12
    'get' 'bone'
    notzero_ FLAG_BOULDER
    present_ bone
    notcarr_ bone
    message_ 79
    done_

    6
    'get' 'bone'
    get_ bone
    desc_

    10
    'get' 'sign'
    present_ sign_broken
    present_ boulder
    message_ 82
    done_

    8
    'get' 'sign'
    present_ sign_broken
    get_ sign_broken
    desc_

    6
    'get' ',_'
    message_ 83
    done_

    6
    'drop' 'deck'
    message_ 84
    done_

    8
    'drop' 'key'
    present_ key
    drop_ key
    desc_

    6
    'drop' 'key'
    drop_ key_broken
    desc_

    6
    'drop' 'tambourine'
    drop_ tambourine
    desc_

    15
    'drop' 'dung'
    carried_ dung
    present_ mule_fly
    swap_ mule_fly mule
    destroy_ dung
    create_ dung_fly
    desc_

    8
    'drop' 'dung'
    present_ dung
    drop_ dung
    desc_

    6
    'drop' 'dung'
    drop_ dung_fly
    desc_

    8
    'drop' 'bag'
    present_ bag_empty
    drop_ bag_empty
    desc_

    6
    'drop' 'bag'
    drop_ bag_flour
    desc_

    13
    'drop' 'torch'
    at_ room20
    present_ torch_unlit
    drop_ torch_unlit
    place_ torch_unlit room22
    ok_

    13
    'drop' 'torch'
    at_ room28
    present_ torch_unlit
    drop_ torch_unlit
    place_ torch_unlit room22
    ok_

    8
    'drop' 'torch'
    present_ torch_unlit
    drop_ torch_unlit
    desc_

    11
    'drop' 'torch'
    at_ room28
    drop_ torch_lit
    place_ torch_lit room22
    ok_

    6
    'drop' 'torch'
    drop_ torch_lit
    desc_

    11
    'drop' 'book'
    at_ room20
    drop_ book
    place_ book room22
    ok_

    11
    'drop' 'book'
    at_ room28
    drop_ book
    place_ book room22
    ok_

    6
    'drop' 'book'
    drop_ book
    desc_

    11
    'drop' 'bone'
    at_ room20
    drop_ bone
    place_ bone room22
    ok_

    11
    'drop' 'bone'
    at_ room28
    drop_ bone
    place_ bone room22
    ok_

    6
    'drop' 'bone'
    drop_ bone
    desc_

    13
    'drop' 'sign'
    present_ sign_broken
    at_ room20
    drop_ sign_broken
    place_ sign_broken room22
    ok_

    8
    'drop' 'sign'
    present_ sign_broken
    drop_ sign_broken
    desc_

    6
    'drop' 'sign'
    drop_ sign
    desc_

    10
    'fill' 'bag'
    at_ room19
    present_ bag_flour
    message_ 85
    done_

    17
    'fill' 'bag'
    at_ room19
    present_ bag_empty
    drop_ bag_empty
    get_ bag_empty
    swap_ bag_empty bag_flour
    message_ 86
    done_

    14
    'fill' 'bag'
    notat_ room19
    present_ bag_empty
    drop_ bag_empty
    get_ bag_empty
    message_ 87
    done_

    8
    'empty' 'bag'
    present_ bag_empty
    message_ 88
    done_

    13
    'empty' 'bag'
    present_ bag_flour
    drop_ bag_flour
    get_ bag_flour
    swap_ bag_flour bag_empty
    ok_

    10
    'wear' 'tambourine'
    drop_ tambourine
    get_ tambourine
    message_ 89
    done_

    6
    'give' 'deck'
    message_ 84
    done_

    21
    'give' 'dung'
    present_ dung_fly
    present_ salamander
    drop_ dung_fly
    get_ dung_fly
    destroy_ salamander
    destroy_ dung_fly
    create_ dung
    message_ 90
    anykey_
    desc_

    27
    'give' 'tambourine'
    present_ mule
    drop_ tambourine
    get_ tambourine
    set_ FLAG_SNAKE
    swap_ snake card3
    destroy_ mule
    destroy_ cart
    destroy_ tambourine
    place_ mule_cart room16
    message_ 91
    anykey_
    desc_

    12
    'give' 'tambourine'
    present_ snake
    drop_ tambourine
    get_ tambourine
    message_ 92
    done_

    14
    'give' 'bag'
    present_ bag_empty
    present_ mule_cart
    drop_ bag_empty
    get_ bag_empty
    message_ 93
    done_

    12
    'give' 'bag'
    present_ mule_cart
    drop_ bag_flour
    get_ bag_flour
    message_ 94
    done_

    15
    'give' 'book'
    present_ hermit
    drop_ book
    plus_ FLAG_CAMPFIRE 1
    destroy_ book
    message_ 95
    done_

    15
    'give' 'sign'
    present_ hermit
    drop_ sign_broken
    plus_ FLAG_CAMPFIRE 1
    destroy_ sign_broken
    message_ 95
    done_

    6
    'give' ',_'
    message_ 96
    done_

    15
    'put' 'bag'
    present_ bag_empty
    drop_ bag_empty
    get_ bag_empty
    Let_ FLAG_PUTIN 2
    message_ 97
    done_

    13
    'put' 'bag'
    drop_ bag_flour
    get_ bag_flour
    Let_ FLAG_PUTIN 2
    message_ 97
    done_

    12
    'cart' ',_'
    notzero_ FLAG_PUTIN
    carried_ bag_empty
    present_ mule_cart
    message_ 93
    done_

    17
    'cart' ',_'
    notzero_ FLAG_PUTIN
    present_ mule_cart
    destroy_ bag_flour
    destroy_ mule_cart
    set_ FLAG_SCORE_PLUS
    message_ 98
    anykey_
    desc_

    13
    'talk' 'kitten'
    at_ room11
    present_ kitten
    Let_ FLAG_YESNO 2
    message_ 99
    done_

    15
    'talk' 'seguin'
    present_ seguin_milking
    clear_ FLAG_CORDE
    destroy_ rope
    destroy_ seguin_milking
    message_ 100
    anykey_
    desc_

    11
    'talk' 'seguin'
    present_ seguin_upseting
    zero_ FLAG_SEGUIN
    set_ FLAG_SEGUIN
    message_ 101

    11
    'talk' 'seguin'
    present_ seguin_upseting
    message_ 102
    Let_ FLAG_YESNO 2
    done_

    19
    'talk' 'rooster'
    present_ rooster
    swap_ rooster rooster_1
    place_ seguin_upseting room8
    message_ 103
    message_ 104
    message_ 105
    anykey_
    desc_

    8
    'talk' 'rooster'
    present_ rooster_1
    message_ 106
    done_

    8
    'talk' 'chick'
    present_ chick
    message_ 107
    done_

    8
    'talk' 'chick'
    present_ chick_1
    message_ 108
    done_

    8
    'talk' 'salamander'
    present_ salamander
    message_ 109
    done_

    10
    'talk' 'elves'
    present_ elves
    message_ 110
    message_ 36
    done_

    8
    'talk' 'mule'
    present_ mule_fly
    message_ 111
    done_

    8
    'talk' 'mule'
    present_ mule
    message_ 111
    done_

    8
    'talk' 'mule'
    present_ mule_cart
    message_ 112
    done_

    8
    'talk' 'snake'
    present_ snake
    message_ 113
    done_

    8
    'talk' 'owl'
    present_ owl
    message_ 114
    done_

    18
    'talk' 'rabbit'
    present_ rabbit_leisurely
    present_ door_opened
    destroy_ rabbit_leisurely
    place_ rabbit_fearful room19
    set_ FLAG_RABBIT
    message_ 115
    anykey_
    desc_

    11
    'talk' 'rabbit'
    present_ rabbit_leisurely
    destroy_ rabbit_leisurely
    message_ 116
    anykey_
    desc_

    15
    'talk' 'rabbit'
    present_ rabbit_fearful
    present_ door_opened
    destroy_ rabbit_fearful
    clear_ FLAG_RABBIT
    message_ 116
    anykey_
    desc_

    19
    'talk' 'rabbit'
    present_ rabbit_fearful
    destroy_ rabbit_fearful
    destroy_ owl
    place_ card1 room16
    let_ FLAG_STORM 1
    message_ 117
    anykey_
    desc_

    14
    'talk' 'hermit'
    at_ room21
    present_ hermit
    zero_ FLAG_NOBODY
    set_ FLAG_NOBODY
    message_ 118
    done_

    10
    'talk' 'hermit'
    at_ room21
    present_ hermit
    message_ 119
    done_

    8
    'talk' 'ant'
    present_ ant
    message_ 120
    done_

    11
    'talk' 'lamb'
    present_ lamb
    zero_ FLAG_LAMB
    set_ FLAG_LAMB
    set_ FLAG_SCORE_PLUS

    8
    'talk' 'lamb'
    present_ lamb
    message_ 121
    done_

    8
    'talk' 'lamb'
    present_ lamb_1
    message_ 122
    done_

    18
    'talk' 'chamois'
    present_ chamois
    notzero_ FLAG_CARD5
    dropall_
    get_ deck
    clear_ FLAG_CARD5
    goto_ room29
    message_ 123
    anykey_
    desc_

    8
    'talk' 'chamois'
    present_ chamois
    message_ 124
    done_

    12
    'talk' 'moth'
    notzero_ FLAG_BOULDER
    at_ room20
    absent_ torch_lit
    message_ 125
    done_

    10
    'talk' 'moth'
    at_ room20
    absent_ torch_lit
    message_ 126
    done_

    6
    'talk' ',_'
    message_ 127
    done_

    14
    'wake' 'kitten'
    present_ kitten_asleep
    notzero_ FLAG_LICK
    swap_ kitten_asleep barrel
    message_ 128
    anykey_
    desc_

    8
    'wake' 'kitten'
    present_ kitten_asleep
    message_ 129
    done_

    8
    'wake' 'hermit'
    present_ hermit_1
    message_ 130
    done_

    13
    'eat' 'daisy'
    present_ daisy
    destroy_ daisy
    minus_ FLAG_EAT 1
    set_ FLAG_SCORE_PLUS
    desc_

    13
    'eat' 'carrot'
    present_ carrot
    destroy_ carrot
    minus_ FLAG_EAT 1
    set_ FLAG_SCORE_PLUS
    desc_

    13
    'eat' 'almond'
    present_ almond
    destroy_ almond
    minus_ FLAG_EAT 1
    set_ FLAG_SCORE_PLUS
    desc_

    8
    'eat' 'salt'
    present_ salt_stone
    message_ 131
    done_

    6
    'eat' ',_'
    message_ 132
    done_

    10
    'lick' 'salt'
    present_ salt_stone
    set_ FLAG_SALT
    message_ 133
    done_

    9
    'lick' 'kitten'
    present_ kitten_asleep
    notzero_ FLAG_SALT
    set_ FLAG_LICK

    8
    'lick' 'kitten'
    present_ kitten_asleep
    message_ 134
    done_

    8
    'drink' ',_'
    at_ room14
    message_ 135
    done_

    8
    'drink' ',_'
    at_ room24
    message_ 135
    done_

    19
    'drink' ',_'
    at_ room23
    present_ lamb
    message_ 136
    destroy_ lamb
    place_ lamb_1 room21
    plus_ FLAG_CAMPFIRE 1
    anykey_
    desc_

    8
    'drink' ',_'
    at_ room23
    message_ 135
    done_


    6
    'drink' ',_'
    message_ 137
    done_

    8
    'open' 'door'
    present_ door_opened
    message_ 138
    done_

    8
    'open' 'door'
    present_ door_locked
    message_ 31
    done_

    9
    'open' 'door'
    present_ door_closed
    swap_ door_closed door_opened
    desc_

    8
    'close' 'door'
    present_ door_closed
    message_ 139
    done_

    8
    'close' 'door'
    present_ door_locked
    message_ 139
    done_

    10
    'close' 'door'
    at_ room7
    present_ door_opened
    message_ 140
    done_

    9
    'close' 'door'
    present_ door_opened
    swap_ door_opened door_closed
    desc_

    8
    'lock' 'door'
    present_ door_locked
    message_ 141
    done_

    8
    'lock' 'door'
    present_ door_opened
    message_ 142
    done_

    10
    'lock' 'door'
    present_ door_closed
    notcarr_ key
    message_ 143
    done_

    9
    'lock' 'door'
    present_ door_closed
    swap_ door_closed door_locked
    desc_

    10
    'unlock' 'door'
    present_ door_locked
    notcarr_ key
    message_ 143
    done_

    14
    'unlock' 'door'
    at_ room16
    present_ door_locked
    swap_ door_locked door_closed
    swap_ key key_broken
    desc_

    9
    'rub' 'card'
    present_ card0
    swap_ card0 card4
    desc_

    6
    'rub' ',_'
    message_ 144
    done_

    21
    'burn' 'torch'
    present_ torch_unlit
    present_ campfire
    drop_ torch_unlit
    get_ torch_unlit
    swap_ torch_unlit torch_lit
    place_ hermit_1 room21
    swap_ campfire ashes
    desc_

    8
    'burn' 'torch'
    present_ torch_unlit
    message_ 145
    done_

    8
    'burn' 'skeleton'
    present_ skeleton
    message_ 146
    done_

    6
    'burn' ',_'
    message_ 147
    done_

    8
    'extinguish' 'torch'
    present_ torch_lit
    message_ 148
    done_

    8
    'extinguish' 'campfire'
    present_ campfire
    message_ 148
    done_

    8
    'break' 'rope'
    present_ rope
    message_ 149
    done_

    6
    'break' ',_'
    message_ 150
    done_

    8
    'attack' ',_'
    atgt_ room29
    message_ 151
    done_

    6
    'attack' ',_'
    message_ 152
    done_

    8
    'move' 'cart'
    present_ cart
    message_ 153
    done_

    8
    'move' 'statue'
    present_ statue
    message_ 154
    done_

    8
    'move' 'barrel'
    present_ kitten_asleep
    message_ 40
    done_

    14
    'move' 'barrel'
    present_ barrel
    absent_ barrel_1
    swap_ barrel barrel_1
    message_ 155
    anykey_
    desc_

    8
    'move' 'kitten'
    present_ kitten_asleep
    message_ 152
    done_

    8
    'move' 'boulder'
    present_ boulder
    message_ 154
    done_

    8
    'move' 'boulder'
    present_ boulder_1
    message_ 154
    done_

    10
    'move' 'boulder'
    at_ room21
    notzero_ FLAG_BOULDER
    message_ 154
    done_

    8
    'move' 'skeleton'
    present_ skeleton
    message_ 156
    done_

    15
    'play' 'tambourine'
    at_ room17
    present_ snake
    carried_ tambourine
    zero_ FLAG_JINGLE
    set_ FLAG_SCORE_PLUS
    set_ FLAG_JINGLE

    17
    'play' 'tambourine'
    at_ room17
    present_ snake
    drop_ tambourine
    get_ tambourine
    destroy_ snake
    message_ 157
    anykey_
    desc_

    10
    'play' 'tambourine'
    drop_ tambourine
    get_ tambourine
    message_ 158
    done_

    12
    'play' 'bone'
    present_ bone
    drop_ bone
    get_ bone
    message_ 159
    done_

    18
    '1//' ',_'
    atgt_ room29
    notzero_ FLAG_CARD4
    clear_ FLAG_CARD4
    let_ FLAG_PLAY 1
    minus_ FLAG_NCARDS 1
    message_ 62
    done_

    10
    '1//' ',_'
    atgt_ room29
    zero_ FLAG_CARD4
    message_ 160
    done_

    18
    '2//' ',_'
    atgt_ room29
    notzero_ FLAG_CARD2
    clear_ FLAG_CARD2
    let_ FLAG_PLAY 2
    minus_ FLAG_NCARDS 1
    message_ 63
    done_

    10
    '2//' ',_'
    atgt_ room29
    zero_ FLAG_CARD2
    message_ 160
    done_

    18
    '3//' ',_'
    atgt_ room29
    notzero_ FLAG_CARD1
    clear_ FLAG_CARD1
    let_ FLAG_PLAY 3
    minus_ FLAG_NCARDS 1
    message_ 64
    done_

    10
    '3//' ',_'
    atgt_ room29
    zero_ FLAG_CARD1
    message_ 160
    done_

    18
    '4//' ',_'
    atgt_ room29
    notzero_ FLAG_CARD3
    clear_ FLAG_CARD3
    let_ FLAG_PLAY 4
    minus_ FLAG_NCARDS 1
    message_ 65
    done_

    10
    '4//' ',_'
    atgt_ room29
    zero_ FLAG_CARD3
    message_ 160
    done_

    24
    'blow' 'bone'
    at_ room24
    present_ boulder
    drop_ bone
    get_ bone
    destroy_ boulder
    swap_ ant boulder_1
    set_ FLAG_BOULDER
    message_ 161
    message_ 162
    anykey_
    desc_

    14
    'blow' 'bone'
    present_ ant
    drop_ bone
    get_ bone
    message_ 161
    message_ 163
    done_

    10
    'blow' 'bone'
    drop_ bone
    get_ bone
    message_ 161
    done_

    6
    'blow' ',_'
    message_ 164
    done_

    11
    'dig' ',_'
    present_ hole
    absent_ cart
    swap_ hole hole_1
    desc_

    8
    'dig' ',_'
    present_ skeleton
    message_ 165
    done_

    6
    'dig' ',_'
    message_ 166
    done_

    8
    'dance' ',_'
    present_ elves
    message_ 167
    done_

    6
    'dance' ',_'
    message_ 168
    done_

    8
    'swim' ',_'
    at_ room14
    message_ 169
    done_

    8
    'swim' ',_'
    at_ room24
    message_ 169
    done_

    8
    'listen' 'dung'
    present_ dung_fly
    message_ 52
    done_

    11
    'listen' ',_'
    at_ room9
    zero_ FLAG_LISTEN
    set_ FLAG_LISTEN
    set_ FLAG_SCORE_PLUS

    8
    'listen' ',_'
    at_ room9
    message_ 170
    done_

    8
    'listen' ',_'
    present_ hole
    message_ 171
    done_

    8
    'listen' ',_'
    present_ hole_1
    message_ 172
    done_

    6
    'listen' ',_'
    message_ 173
    done_

    8
    'smell' 'dung'
    present_ dung
    message_ 174
    done_

    8
    'smell' 'dung'
    present_ dung_fly
    message_ 174
    done_

    8
    'smell' ',_'
    at_ room6
    message_ 175
    done_

    8
    'smell' ',_'
    present_ campfire
    message_ 176
    done_

    6
    'smell' ',_'
    message_ 177
    done_

    6
    'sleep' ',_'
    message_ 178
    done_

    6
    'wait' ',_'
    message_ 179
    done_

    6
    'ask' ',_'
    message_ 180
    done_

    6
    'use' ',_'
    message_ 181
    done_

    21
    'yes' ',_'
    notzero_ FLAG_YESNO
    at_ room11
    present_ kitten
    destroy_ kitten
    swap_ card0 chick
    swap_ tree tree_1
    message_ 182
    anykey_
    desc_

    20
    'yes' ',_'
    notzero_ FLAG_YESNO
    at_ room8
    present_ seguin_upseting
    place_ door_locked room12
    place_ salt_stone room12
    clear_ FLAG_LICK
    goto_ room12
    desc_

    28
    'yes' ',_'
    notzero_ FLAG_YESNO
    at_ room18
    dropall_
    get_ deck
    goto_ room20
    set_ FLAG_DARK
    clear_ FLAG_SHORTCUT
    clear_ FLAG_BOULDER
    clear_ FLAG_PATH
    clear_ FLAG_NOBODY
    clear_ FLAG_CAMPFIRE
    message_ 183
    anykey_
    desc_

    8
    'help' ',_'
    cls_
    message_ 184
    anykey_
    desc_

    6
    'about' ',_'
    message_ 185
    done_

    6
    'verbs' ',_'
    message_ 186
    done_

    8
    'i//' ',_'
    zero_ FLAG_INV
    Let_ FLAG_INV 1

    4
    'i//' ',_'
    inven_

    4
    'l//' ',_'
    desc_

    5
    'script' ',_'
    script_
    done_

    6
    'score' ',_'
    score_
    turns_
    done_

    6
    'q//' ',_'
    quit_
    turns_
    end_

    8
    'save' ',_'
    atgt_ room28
    message_ 187
    done_

    4
    'save' ',_'
    save_

    4
    'load' ',_'
    load_
    0
;
