Array messages-->
!#0
	"^Seguin: Hi Blanquette, my pretty young goat, come and see me in the stable when you have found and eaten a vegetable, a flower and a fruit that will make your milk fat and fragrant."
!#1
	"^[Type HELP if this is your first time playing this game.]"
!#2
	"^Seguin: Hey! Blanquette, come back to me when you have eaten a vegetable, a flower and a fruit."
!#3
	"Blanquette climbs onto the barrel, and jumps out of the window."
!#4
	"A violent storm suddenly breaks out, pouring a torrential rain that makes the river overflow, washing away part of the bridge that crosses it.^^The party is over, the elves have gone home."
!#5
	"^Hermit: I've got everything I need. See you later."
!#6
	"^It's your turn Blanquette, choose a card from 1 to 4 in your deck."
!#7
	"^The mongoose fends off Kitten's attack."
!#8
	"^No one won the round."
!#9
	"^The mongoose is poisoned by the mushroom."
!#10
	"^Blanquette won the round."
!#11
	"^A 100 t hammer comes out of the joker card and hits the wolf."
!#12
	"^The snake is eaten by the mongoose."
!#13
	"^Blanquette lost the round."
!#14
	"^The bird of prey grabs and carries off Kitten."
!#15
	"^The bird of prey ignores the mushroom."
!#16
	"^The snake is eaten by the bird of prey!"
!#17
	"^Blanquette lose the round."
!#18
	"^The joker dodges the attack."
!#19
	"^The kitten lacerates the ectoplasm with its claws."
!#20
	"^The ectoplasm feasts on the mushroom."
!#21
	"^The ectoplasm dodges the snake's attack."
!#22
	"^[The score has gone up by 10 points.]"
!#23
	"^Type X DECK to display the contents of your deck."
!#24
	"Kitten: Too bad! See you later."
!#25
	"Seguin: Get out of my vegetable garden, then!"
!#26
	"I can't, since the rope is holding me back."
!#27
	"I can't, since the door is closed."
!#28
	"I can't, since the boulder is in the way."
!#29
	"As Blanquette begins to ascend the mountain, a huge boulder comes loose, crushing everything in its path."
!#30
	"The path is too dangerous to ascend it without an experienced guide."
!#31
	"I can't, since the door is locked."
!#32
	"You can't run away! You have to fight."
!#33
	"I can't, since the fence is in the way."
!#34
	"I can't reach the window from here."
!#35
	"Elf: Hey! Blanquette, where are you going? Comes to dance with us."
!#36
	"^Blanquette is dragged into the farandole, and comes out dazed."
!#37
	"I can't, since part of the bridge has been washed away by the river."
!#38
	"I found a shortcut through a winding path covered in brush!"
!#39
	"I found a shortcut through a narrow, low passage."
!#40
	"I must wake Kitten first."
!#41
	"Blanquette jumps on the barrel, peers out of the window on the opposite wall, then climbs back down."
!#42
	"I don't jump as far as the springbok."
!#43
	"Blanquette gives a kick."
!#44
	"I can't, since the hole is under the overturned cart."
!#45
	"I can't, since the hole is too narrow."
!#46
	"Underground voice: Did you do everything you intended to do around here? (Y/N)"
!#47
	"The key is at the bottom of the basin, among the dead leaves."
!#48
	"A large antique key weakened by rust."
!#49
	"All that's left is to make an identical key."
!#50
	"A circular percussion instrument with pairs of small metal jingles."
!#51
	"A high-quality flytrap."
!#52
	"There's a lot of buzz around it!"
!#53
	"The burlap sack is used as a blanket by the owl."
!#54
	"The bag is empty, and just needs to be filled."
!#55
	"The bag is full, and ready to go."
!#56
	"A flaming torch to see in the dark."
!#57
	"It's just waiting to be lighted."
!#58
	"It says: Great Kitchen Dictionary by Alexandre Dumas."
!#59
	"A pierced bone from a goat's leg."
!#60
	"It seems suitable for firewood."
!#61
	"The deck contains:^ Basic rules"
!#62
	" [1] The Kitten card"
!#63
	" [2] The Toadstool card"
!#64
	" [3] The Four-leaf Clover card"
!#65
	" [4] The Venomous Snake card"
!#66
	" [5] The Morning Star card"
!#67
	"The card seems cursed, and I have no way of blessing it."
!#68
	"With this card added to your deck, the final battle will be in your favour."
!#69
	"With this card added to your deck, you're ready for the final battle."
!#70
	"You can only usefully examine objects surrounded by (*).^Type HELP, please."
!#71
	"It says: When Blanquette has to fight her destiny, choose an available card from your deck, and type the number associated with it."
!#72
	"You're obedient, which is good."
!#73
	"It's says: Risk of falling rocks!"
!#74
	"There's a recipe for lamb cooked over a wood fire."
!#75
	"It says: There must be another entrance somewhere."
!#76
	"I'd rather not, as it could have a negative effect on my deck."
!#77
	"I can't reach it from here."
!#78
	"^Salamander: I'll get it for you if you bring me some flies to nibble on."
!#79
	"I can leave it there, since I don't need it anymore."
!#80
	"Elf: You can have it when the party's over."
!#81
	"Hermit: Hey! Nobody, you know well that I hate anybody touching my stuff."
!#82
	"I can't, since the sign is stuck under the boulder."
!#83
	"You can only get objects surrounded by (*).^Type HELP, please."
!#84
	"You can't, since Blanquette need it for the final fight."
!#85
	"The bag is already full of flour."
!#86
	"You fill the bag with the flour from the millstone."
!#87
	"There isn't anything obvious with which to fill it."
!#88
	"The bag is already empty."
!#89
	"I'd rather not. I'll never again be restrained by a collar and leash, rope or chain."
!#90
	"The flies rush to the salamander, which swallows them at once. The salamander vanishes under the leaves after having brought back the key."
!#91
	"Mule: Thanks! This will make a pretty bell collar. See you later."
!#92
	"Snake: I'm not a pet to be tied up or caged!"
!#93
	"But the bag is empty at the moment."
!#94
	"Mule: PUT it in the cart."
!#95
	"Hermit: Thanks! Nobody. And what else?"
!#96
	"No one seems interested."
!#97
	"In what?"
!#98
	"Mule: Thanks! See you later."
!#99
	"Kitten: Hi Blanquette, I can't see my beloved sister from up here. Do you know where she's hidden? (Y/N)"
!#100
	"Seguin: Congratulations Blanquette, your milk is of very good quality. Now that my bucket is full, I'm going to take a little nap."
!#101
	"Seguin: A rabbit stole my carrots again! Hey! Blanquette, what's going on?^^*: I want to go to the mountain, Mr Seguin.^^Seguin: But don't you know that there's a wolf in the mountain? What will you do when he comes?^^*: It doesn't matter, Mr Seguin, let me go to the mountain.^^Seguin: I will lock you in the stable, and you will stay there!^"
!#102
	"Seguin: Did you do everything you intended to do around here? (Y/N)"
!#103
	"Rooster: Hi Blanquette, the wild freedom is on the other side of the wall. LISTEN to the mountain breath, and you will know the great price of freedom."
!#104
	"^*: How good it must be up there! Without fences or rope that skin my neck."
!#105
	"^Rooster: By the way, I saw Mr Seguin in the vegetable garden."
!#106
	"Rooster: COCK-A-DOODLE-DO!"
!#107
	"Chick: Hi Blanquette, if you see my brother Kitten, don't tell him where I'm hiding!"
!#108
	"Chick: CHEEP-CHEEP!"
!#109
	"Salamander: Hi Blanquette, you shouldn't be here, Mr Seguin will be worried."
!#110
	"Elf: Hi Blanquette, we're celebrating the Shepherd's day, come to dance with us."
!#111
	"Mule: Hi Blanquette, I have to fetch the wrecked hay wagon, but a nasty snake is in my way. Oh, if only I had my bell collar to scare him off!"
!#112
	"Mule: Hi Blanquette, I have to bring a bag of flour to Mr Seguin. If you have one, PUT it in the cart."
!#113
	"Snake: Hi blanquette, the mule, far too clumsy to take the shortcut, nearly ran me over as I passed. Next time, I'll bite her!"
!#114
	"Owl: Hi blanquette, I see little rabbits grazing on the clover outside the windmill, but I'm too old to chase them."
!#115
	"The scared rabbit runs inside windmill at high speed."
!#116
	"The scared rabbit runs away at high speed."
!#117
	"The frightened rabbit runs away at high speed, and hits the closed door.^^Owl: Hey! what's going on?^^The owl dives on the stunned rabbit, and carries its prey to safety.^^The burlap bag fell off the roof framework."
!#118
	"Hermit: Hi Nobody, bring me some stale bread, wood and paper so I can make a campfire toast.^^*: I'm not Nobody, I'm Blanquette!^^Hermit: Ha ha ha! And I'm Polypheme! What a joker this Nobody."
!#119
	"Hermit: Hi Nobody, bring me some stale bread, wood and paper so I can make a campfire toast."
!#120
	"Ant: Hi Blanquette, the hermit and his lamb trample us every time they come out of the cave. If you find anything to block the entrance, let me know where by blowing a loud whistle."
!#121
	"Robin: Hi Blanquette, I'm sure the hermit wants to eat me to atone for his sins!"
!#122
	"Lamb: BAA MAA!"
!#123
	"Chamois: It's time to face your destiny, Blanquette.^^The deck of cards shakes and lights up, the Morning Star card springs up to join the firmament. It's pitch-dark now."
!#124
	"Chamois: Hi Blanquette, find the Morning Star card, and I'll lead you on the path to your destiny.^Don't forget to SAVE before the final battle."
!#125
	"Moth: ..."
!#126
	"Moth: Hi Blanquette, I saw a lamb runs away at high speed to the west, south and west again."
!#127
	"There's no reply."
!#128
	"Kitten stretches, licks his paw, and disgusted by the taste of salt, runs away through the window."
!#129
	"Kitten stretches, licks his paw to clean his hair, and falls back to sleep."
!#130
	"Nothing can wake him up."
!#131
	"It's not for eating, it's for licking."
!#132
	"It's not my favourite meal."
!#133
	"My tongue is now full of salt."
!#134
	"Kitten purrs with pleasure in his sleep."
!#135
	"Blanquette quenches its thirst, and splashes with fresh water."
!#136
	"The lamb seems hypnotized by the water's reflection of Blanquette.^^Robin: Hey! Blanquette, what big eyes you have! What big ears you have! What big teeth you have!^^The scared lamb runs away at high speed."
!#137
	"There's nothing suitable to drink here."
!#138
	"The door is already open."
!#139
	"The door is already closed."
!#140
	"Seguin: Hey! Blanquette, leave this door open."
!#141
	"The door is already locked."
!#142
	"I can't, since the door is opened."
!#143
	"I need a key."
!#144
	"I achieve nothing by this."
!#145
	"There's nothing here to light it."
!#146
	"Renaude didn't want to be cremated."
!#147
	"This dangerous act would achieve little."
!#148
	"The flame dies, and is reborn again."
!#149
	"I can't, since the rope is strong and the knot very tight."
!#150
	"I could get hurt."
!#151
	"Use your cards to fight."
!#152
	"Violence isn't the answer to this one."
!#153
	"I don't have enough strength to lift it."
!#154
	"I don't have enough strength to move it."
!#155
	"The barrel rolls under the window."
!#156
	"Leave her in peace. The vultures will take care of it."
!#157
	"~TING-A-LING!~^^At the sound of the jingle, the snake runs away at high speed."
!#158
	"~TING-A-LING!~"
!#159
	"Try to BLOW into it instead."
!#160
	"This card is not in your deck."
!#161
	"~TWEET TWEET TWEET!~"
!#162
	"^The ground darkened and began to move, dragging the rock with it.^^Ant: Thanks! See you later."
!#163
	"^Ant: Blow a loud whistle where you find anything to block the entrance."
!#164
	"~TWEET!~"
!#165
	"Renaude didn't want to be buried."
!#166
	"Digging would achieve nothing here."
!#167
	"Blanquette is dragged into the farandole, and comes out dazed."
!#168
	"Blanquette move in a graceful and rhythmical way."
!#169
	"The river's current is too strong."
!#170
	"I hear a wolf howling in the distance."
!#171
	"Underground voice: BAA MAA!"
!#172
	"~BAA MAA!~"
!#173
	"I hear nothing unexpected."
!#174
	"It doesn't smell like roses."
!#175
	"A subtle flowery fragrance."
!#176
	"Robin's body odour."
!#177
	"I smell nothing unexpected."
!#178
	"I'm not feeling especially drowsy."
!#179
	"Nothing happens."
!#180
	"Use TALK to communicate with characters."
!#181
	"You don't need to use this verb in this game."
!#182
	"Kitten: Thanks! See you later."
!#183
	"Blanquette is sucked down the rabbit hole, and falls back into a cave, onto a lamb that cushions her fall."
!#184
	"The game expect commands in the form of VERB NOUN (not case-sensitive).^^Basic commands:^L or R to refresh the location display.^N, S, E, W, U & D to move around.^I to display what you're carrying and wearing.^X to examine an object.^GET & DROP to pick up or drop an object.^TALK to communicate with a character.^VERBS to display commands needed to complete the game.^SAVE & RESTORE to save or restore the game.^^Interact only with objects displayed after: I notice.^EXAMINE (X) and GET are only useful for objects surrounded by (*) e.g. A *key*."
!#185
	"Mr Seguin's Goat (c) 2023 Lionel Ange.^Based on a short story in Letters from My Windmill (1869), by the French writer and playwright Alphonse Daudet.^Playtesting by Nobody and Polypheme.^^Release 1.2 / Serial number 231215 / zQuill 1.4^The game code is compatible with The Quill (Gilsoft).^^This game was created for Text Adventure Literacy Jam in May 2023."
!#186
	"Some useful commands:^L (look), I (inventory)^X (examine), READ^N, S, E, W, U, D, CLIMB, JUMP, GO^TALK, GIVE, WAKE, EAT, DRINK, LICK, BLOW^GET, DROP, PUT, WEAR, REMOVE, FILL, EMPTY^OPEN, CLOSE, UNLOCK, MOVE, DIG^LIGHT, EXTINGUISH, CLEAN, PLAY, LISTEN^SAVE, RESTORE, YES, NO, SCORE^HELP, ABOUT, VERBS^^And other commands (or synonyms) that you will discover by yourself."
!#187
	"It's too late to save. Blanquette must face her destiny."
; !End_Array
