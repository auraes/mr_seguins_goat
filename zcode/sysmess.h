Constant INK_DEFAULT CLR_CURRENT;
Constant PAPER_DEFAULT CLR_CURRENT;
Constant PROMPT ">";
Constant YES 'y';
Constant CENTERING_SYSMESS_16 25;

Array sysmess-->
!0
   "It's pitch dark here! I can't see a thing.^^ A moth swirls around Blanquette^"
!1
   "^I notice:^"
!2
   "^"
!3
   "^"
!4
   "^"
!5
   "^"
!6
   "Sorry, I don't understand that. Try some different words.^"
!7
   "I can't go that way.^"
!8
   "I can't.^"
!9
   "You're carrying:^"
!10
   " (worn)"
!11
   " Nothing at all.^"
!12
   "Do you really want to quit now?^"
!13
   "^END OF GAME^^Thanks for playing. Do you want to try again?^"
!14
   "Bye. Have a nice day.^"
!15
   "OK.^"
!16
   "Press any key to continue"
!17
   "You have taken "
!18
   " turn"
!19
   "s"
!20
   ".^"
!21
   "You have scored "
!22
   "%^"
!23
   "I'm not wearing it.^"
!24
   "You can't. Your hands are full.^"
!25
   "You already have it.^"
!26
   "It's not here.^"
!27
   "You can't carry any more.^"
!28
   "You don't have it.^"
!29
   "I'm already wearing it.^"
!30
   "Y^"
!31
   "N^"
!32
   "Save failed.^"
!33
   "Restore failed.^"
!34
   "^"
; !End_Array
