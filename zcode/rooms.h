Constant START_ROOM room0;

Object room0
with
   describe "The goats of Mr Seguin liked the open air and freedom... They were all eaten by the wolf.^^Mr Seguin's Goat^A text adventure game, based on a story by Alphonse Daudet.^(c) 2023 Auraes. Type ABOUT for more information.^^Release 1.2 / Serial number 231215 / zQuill 1.4";

Object room1
with
   describe "I'm in a meadow covered with tall bitter grass.^Exit: north",
   exits 'n//' room4;

Object room2
with
   describe "I'm in the dry, stony orchard.^I can see fruits fallen on the ground.^Exit: east",
   exits 'e//' room3;

Object room3
with
   describe "I'm in the lush grass of a meadow.^I can see the orchard to the west, and a sign that says: Type READ SIGN, please.^Exits: n, e, w",
   exits 'n//' room6 'e//' room4 'w//' room2;

Object room4
with
   describe "I'm in a meadow surrounded by hawthorns.^I can see a large stake stuck in the ground, and the stable to the north.^Exits: n, s, w",
   exits 'n//' room7 's//' room1;

Object room5
with
   describe "I'm in a weedy meadow.^I can see the vegetable garden to the north.^Exit: north",
   exits 'n//' room8;

Object room6
with
   describe "I'm in a wildflower meadow.^I can see the farmyard to the north, and the stable to the east.^Exits: n, s, e";

Object room7
with
   describe "I'm in front of the horse stable.^I can see the entrance door to the north, and the fenced-in vegetable garden to the east.^Exits: n, s, e, w",
   exits 's//' room4 'w//' room6;

Object room8
with
   describe "I'm in the vegetable garden.^I can see many varieties of vegetables, a picket fence and the stable to the west.^Exits: s, w";

Object room9
with
   describe "I'm in the farmyard, behind the stable.^I can see a small mountain range over the surrounding wall.^Exit: south",
   exits 's//' room6;

Object room10
with
   describe "I'm in a spacious and airy horse stable.^Exit: south";

Object room11
with
   describe "I'm at the top of a dead tree.^I can see a windmill silhouette in the distance, towards the north.^Exit: down",
   exits 'd//' room2;

Object room12
with
   describe "I'm in a double-locked horse stable.^I can see a large open window at the upper end of the east wall.^Exit: s, e";

Object room13
with
   describe "I'm beside an old washhouse, at the back of Mr Seguin's house.^I can see a large weathered stone basin.^Exit: north",
   exits 'n//' room14;

Object room14
with
   describe "I'm on the left bank of a river, crossed by a bridge to the east.^I can see the washhouse to the south.^Exits: s, e, w",
   exits 's//' room13 'w//' room15;

Object room15
with
   describe "I'm on a small grassy area surrounded by hills.^I can see a windmill overlooking the landscape to the north, and the river to the east.^Exits: n, e",
   exits 'n//' room16 'e//' room14;

Object room16
with
   describe "I'm on top of a windy hill, outside a derelict windmill.^I can see the front door to the north.^Exits: n, s, w",
   exits 's//' room15 'w//' room17;

Object room17
with
   describe "I'm on a steep path that winds between the rocks.^I can see a rough trail leading down to the south, and the windmill to the east.^Exits: s, e",
   exits 'e//' room16 's//' room18;

Object room18
with
   describe "I'm on the edge of a precipice overlooking a wide, green valley.^Exit: north";

Object room19
with
   describe "I'm in a tower-shaped windmill.^I can see a big pile of flour on a grinding stone.^Exit: south";

Object room20
with
   describe "I'm in the depths of a dark cave.^Exit: west";

Object room21
with
   describe "I'm in a rustically furnished cave.^I can see the outside light coming from the south.^Exits: s, e";

Object room22
with
   describe "I'm in front of the north entrance to a cave.^I can see a huge anthill of dried mud and twigs.^Exits: n, w",
   exits 'w//' room23;

Object room23
with
   describe "I'm downstream of the river, on former pastures that have gone wild again.^I can see a cave to the east, and the north face of a mountain in the distance.^Exits: n, e",
   exits 'n//' room24 'e//' room22;

Object room24
with
   describe "I'm on the right bank of a river, crossed by a bridge to the west. ^I can see a path leading up the north side of the mountain.^Exits: n, s, w",
   exits 'n//' room25 's//' room23;

Object room25
with
   describe "I'm on an arid mountain plateau with sparse vegetation.^I can see a forest to the east, and the lowland to the south.^Exits: s, e, w",
   exits 's//' room24 'e//' room27 'w//' room26;

Object room26
with
   describe "I'm on a headland overlooking the valley.^In the distance, through the mist, I can see Mr Seguin's house.^Exit: east",
   exits 'e//' room25;

Object room27
with
   describe "I'm in a quiet grove of holm oaks.^I can see a steep, narrow path rising to the north.^Exits: n, w",
   exits 'w//' room25;

Object room28
with
   describe "It's bright out here! I can't see a thing.",
   exits 'w//' room23;

Object room29
with
   describe "I'm on top of a moonlit mountain, in the menacing shadow of tall, ghostly trees.^I can see two bright little eyes piercing the night.^^Wolf: Hi Blanquette, here you're at last!^^The wolf gets up, and howls at the moon, signalling the start of the fight.^^It's showtime!";

Object room30
with
   describe "I'm on top of a moonlit mountain, in the menacing shadow of tall, ghostly trees.^^The wolf curls up his lips over his fangs, and plays:^^ The Mongoose card";

Object room31
with
   describe "I'm on top of a moonlit mountain, in the menacing shadow of tall, ghostly trees.^^The wolf leaps up, sharp claws out, and plays:^^ The Bird of Prey card";

Object room32
with
   describe "I'm on top of a moonlit mountain, in the menacing shadoaw of tall, ghostly trees.^^The wolf rolls to the ground, then leaps up, and plays:^^ The Joker card";

Object room33
with
   describe "I'm on top of a moonlit mountain, in the menacing shadow of tall, ghostly trees.^^The wolf runs around Blanquette, attacks by surprise, and plays:^^ The Ectoplasm card";

Object room34
with
   describe "I'm on top of a moonlit mountain, in the menacing shadow of tall, ghostly trees.^^Wolf: What a shame, your deck is empty. Ha ha ha! What an unfair world.^^The wolf plays:^^The Angry Wolf card^^The wolf pounces on Blanquette, carries her off, and then eats her.";

Object room35
with
   describe "Rooster: COCK-A-DOODLE-DO!^^Congratulations, you've fought the wolf until dawn.^^YOU'VE WON!^";

Object room36
with
   describe "You fought well, but the wolf ate you before dawn.^^YOU'VE LOST!^";

