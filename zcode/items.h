Constant CAPACITY 4;
Constant LIGHT_OBJECT torch_lit;

!0
Object torch_lit
with
   describe " A lit *torch*",
   at not_created;
!1
Object seguin
with
   describe " Mr Seguin looking happy",
   at room4;
!2
Object seguin_cleaning
with
   describe " Mr Seguin cleaning the stable",
   at room10;
!3
Object seguin_milking
with
   describe " Mr Seguin waiting for the milking",
   at not_created;
!4
Object seguin_upseting
with
   describe " Mr Seguin looking upset",
   at not_created;
!5
object ashes
with
   describe " Ashes of a campfire",
   at room25;
!6
Object ant
with
   describe " A worker ant",
   at room22;
!7
Object boulder
with
   describe " A large boulder",
   at not_created;
!8
Object boulder_1
with
   describe " An engraved boulder",
   at not_created;
!9
object campfire
with
   describe " A campfire smelling of roast lamb",
   at not_created;
!10
Object carrot
with
   describe " A fleshy carrot",
   at room8;
!11
Object cart
with
   describe " An overturned hay cart",
   at room18;
!12
Object chamois
with
   describe " An alluring chamois",
   at not_created;
!13
Object chick
with
   describe " A cute chick playing hide-and-seek",
   at room1;
!14
Object chick_1
with
   describe " A cute chick",
   at not_created;
!15
Object daisy
with
   describe " A little daisy",
   at room6;
!16
Object owl
with
   describe " An old owl perching on the roof frame",
   at room19;
!17
Object door_locked
with
   describe " A locked door",
   at not_created;
!18
Object door_closed
with
   describe " A closed door",
   at not_created;
!19
Object door_opened
with
   describe " A wide open door",
   at room7;
!20
Object barrel
with
   describe " An empty barrel",
   at room10;
!21
Object elves
with
   describe " Elves dancing around a shepherd statue",
   at room14;
!22
Object statue
with
   describe " A shepherd statue",
   at not_created;
!23
Object fence
with
   describe " A low picket fence",
   at room7;
!24
Object hermit
with
   describe " A blind hermit dressed in a sheepskin",
   at room21;
!25
Object hermit_1
with
   describe " The hermit sleeping soundly",
   at not_created;
!26
Object hole
with
   describe " A small rabbit hole",
   at room18;
!27
Object hole_1
with
   describe " A wide and deep rabbit hole",
   at not_created;
!28
Object kitten
with
   describe " Kitten scanning the meadow for a chick",
   at room11;
!29
Object kitten_asleep
with
   describe " Kitten sleeping on an empty barrel",
   at room12;
!30
Object lamb
with
   describe " A lamb quenching its thirst",
   at room23;
!31
Object lamb_1
with
   describe " An anxious lamb",
   at not_created;
!32
Object mule_fly
with
   describe " A mule keeping flies away with his tail",
   at room15;
!33
Object mule
with
   describe " A mule grazing peacefully",
   at not_created;
!34
Object mule_cart
with
   describe " A mule hitched to a cart",
   at not_created;
!35
Object puddle
with
   describe " A puddle of water reflecting the sky",
   at room23;
!36
Object rabbit_fearful
with
   describe " A fearful rabbit",
   at not_created;
!37
Object rabbit_leisurely
with
   describe " A leisurely rabbit",
   at room16;
!38
Object rooster
with
   describe " A young rooster puffing out his chest",
   at room9;
!39
Object rooster_1
with
   describe " A young rooster",
   at not_created;
!40
Object rope
with
   describe " A long rope tied to a stake",
   at room4;
!41
Object salamander
with
   describe " A yellow-spotted salamander",
   at room13;
!42
Object salt_stone
with
   describe " A salt stone fixed to the wall",
   at room10;
!43
Object sign
with
   describe " A warning sign.",
   at room24;
!44
Object skeleton
with
   describe " The Renaude goat's skeleton",
   at room27;
!45
Object snake
with
   describe " A poisonous snake standing upright",
   at room17;
!46
Object tree
with
   describe " A tree struck by lightning^ The shadow of a perched animal",
   at room2;
!47
Object tree_1
with
   describe " A tree struck by lightning",
   at not_created;
!48
Object track
with
   describe " An animal's tracks",
   at room26;
!49
Object almond
with
   describe " A soft-shelled almond",
   at room2;
!50
Object bag_empty
with
   describe " An empty *bag*",
   at room19;
!51
Object bag_flour
with
   describe " A *bag* full of flour",
   at not_created;
!52
Object card0
with
   describe " A bloody *card*",
   at not_created;
!53
Object card1
with
   describe " The Four-leaf Clover *card*",
   at not_created;
!54
Object card2
with
   describe " The Toadstool *card*",
   at room5;
!55
Object card3
with
   describe " The Venomous Snake *card*",
   at not_created;
!56
Object card4
with
   describe " The Kitten *card*",
   at not_created;
!57
Object card5
with
   describe " The Morning Star *card*",
   at room20;
!58
Object bone
with
   describe " A broken *bone*",
   at not_created;
!59
Object book
with
   describe " A *cookbook*",
   at room25;
!60
Object dung
with
   describe " A mule *dung*",
   at room18;
!61
Object dung_fly
with
   describe " A *dung* full of flies",
   at not_created;
!62
Object key
with
   describe " A rusty *key*",
   at room13;
!63
Object key_broken
with
   describe " A broken *key*",
   at not_created;
!64
Object tambourine
with
   describe " An headless *tambourine*",
   at room14;
!65
Object sign_broken
with
   describe " A broken wooden *sign*",
   at not_created;
!66
Object torch_unlit
with
   describe " An unlit *torch*",
   at room21;
!67
Object deck
with
   describe " A deck of playing cards",
   at at_carried;
!68
Object barrel_1
with
   describe " A barrel out of place",
   at not_created;
