Constant FLAG_DARK        0;
Constant FLAG_YESNO       5;
Constant FLAG_PUTIN       6;

Constant FLAG_START       11;
Constant FLAG_CORDE       12;
Constant FLAG_EAT         13;
Constant FLAG_OBEDIENT    14;
Constant FLAG_SEGUIN      15;
Constant FLAG_LISTEN      16;

Constant FLAG_PLAY        19;
Constant FLAG_TURN        20;
Constant FLAG_MAX         21;
Constant FLAG_INV         22;
Constant FLAG_SCORE_PLUS  23;
Constant FLAG_NCARDS      24;
Constant FLAG_CARD1       25;
Constant FLAG_CARD2       26;
Constant FLAG_CARD3       27;
Constant FLAG_CARD4       28;
Constant FLAG_CARD5       29;
Constant FLAG_SCORE       30;

Constant FLAG_BARREL      12;
Constant FLAG_SALT        13;
Constant FLAG_LICK        14;

Constant FLAG_SHORTCUT    12;
Constant FLAG_RABBIT      13;
Constant FLAG_SNAKE       14;
Constant FLAG_JINGLE      15;
Constant FLAG_STORM       16;

Constant FLAG_BOULDER     13;
Constant FLAG_PATH        14;
Constant FLAG_NOBODY      15;
Constant FLAG_CAMPFIRE    16;
Constant FLAG_LAMB        17;

Array StatusTable table
#
* *
at room0
set FLAG_CORDE
let FLAG_EAT 4
anykey
goto room4
desc

#
* *
at room4
zero FLAG_START
let FLAG_START 1
destroy seguin
"^Seguin: Hi Blanquette, my pretty young goat, come and see me in the stable when you have found and eaten a vegetable, a flower and a fruit that will make your milk fat and fragrant."
anykey
desc

#
* *
at room4
eq FLAG_START 1
set FLAG_START
"^[Type HELP if this is your first time playing this game.]"

#
* *
at room4
notzero FLAG_CORDE
set FLAG_CORDE

#
* *
at room10
notzero FLAG_CORDE
"^Seguin: Hey! Blanquette, come back to me when you have eaten a vegetable, a flower and a fruit."
anykey
goto room7
desc

#
* *
eq FLAG_EAT 1
clear FLAG_EAT
place seguin_milking room4
swap door_opened door_locked

#
* *
at room12
notzero FLAG_BARREL
clear FLAG_SHORTCUT
clear FLAG_RABBIT
clear FLAG_SNAKE
clear FLAG_JINGLE
clear FLAG_LISTEN
place door_locked room16
"Blanquette climbs onto the barrel, and jumps out of the window."
anykey
goto room13
desc

#
* *
at room16
zero FLAG_SNAKE
place snake room17

#
* *
at room18
zero FLAG_SNAKE
place snake room17

#
* *
at room18
zero FLAG_RABBIT
place rabbit_leisurely room16

#
* *
eq FLAG_STORM 1
at room16
set FLAG_STORM
swap elves statue
cls
"A violent storm suddenly breaks out, pouring a torrential rain that makes the river overflow, washing away part of the bridge that crosses it.^^The party is over, the elves have gone home."
anykey
desc

#
* *
at room21
eq FLAG_CAMPFIRE 3
clear FLAG_CAMPFIRE
destroy hermit
swap ashes campfire
destroy lamb_1
"^Hermit: I've got everything I need. See you later."
anykey
desc

#
* *
at room29
eq FLAG_NCARDS 4
set FLAG_MAX

#
* *
at room29
goto room30
set FLAG_TURN
anykey
desc

#
* *
atgt room29
notzero FLAG_TURN
clear FLAG_TURN
"^It's your turn Blanquette, choose a card from 1 to 4 in your deck."

#
* *
at room30
eq FLAG_PLAY 1
"^The mongoose fends off Kitten's attack."
"^No one won the round."

#
* *
at room30
eq FLAG_PLAY 2
"^The mongoose is poisoned by the mushroom."
"^Blanquette won the round."

#
* *
at room30
eq FLAG_PLAY 3
"^A 100 t hammer comes out of the joker card and hits the wolf."
"^Blanquette won the round."

#
* *
at room30
eq FLAG_PLAY 4
"^The snake is eaten by the mongoose."
"^Blanquette lost the round."

#
* *
at room30
notzero FLAG_PLAY
clear FLAG_PLAY
set FLAG_TURN
goto room31
anykey
desc

#
* *
at room31
eq FLAG_PLAY 1
"^The bird of prey grabs and carries off Kitten."
"^Blanquette lost the round."

#
* *
at room31
eq FLAG_PLAY 2
"^The bird of prey ignores the mushroom."
"^No one won the round."

#
* *
at room31
eq FLAG_PLAY 3
"^A 100 t hammer comes out of the joker card and hits the wolf."
"^Blanquette won the round."

#
* *
at room31
eq FLAG_PLAY 4
"^The snake is eaten by the bird of prey!"
"^Blanquette lose the round."

#
* *
at room31
notzero FLAG_PLAY
clear FLAG_PLAY
set FLAG_TURN
goto room32
anykey
desc

#
* *
at room32
eq FLAG_PLAY 1
"^The joker dodges the attack."
"^No one won the round."

#
* *
at room32
eq FLAG_PLAY 2
"^The joker dodges the attack."
"^No one won the round."

#
* *
at room32
eq FLAG_PLAY 3
"^The joker dodges the attack."
"^No one won the round."

#
* *
at room32
eq FLAG_PLAY 4
"^The joker dodges the attack."
"^No one won the round."

#
* *
at room32
notzero FLAG_PLAY
clear FLAG_PLAY
set FLAG_TURN
goto room33
anykey
desc

#
* *
at room33
eq FLAG_PLAY 1
"^The kitten lacerates the ectoplasm with its claws."
"^Blanquette won the round."

#
* *
at room33
eq FLAG_PLAY 2
"^The ectoplasm feasts on the mushroom."
"^Blanquette lost the round."

#
* *
at room33
eq FLAG_PLAY 3
"^A 100 t hammer comes out of the joker card and hits the wolf."
"^Blanquette won the round."

#
* *
at room33
eq FLAG_PLAY 4
"^The ectoplasm dodges the snake's attack."
"^No one won the round."

#
* *
at room33
notzero FLAG_PLAY
goto room34
anykey
desc

#
* *
atgt room29
zero FLAG_NCARDS
set FLAG_NCARDS
goto room34
desc

#
* *
at room34
notzero FLAG_MAX
goto room35
anykey
desc

#
* *
at room34
goto room36
anykey
desc

#
* *
at room35
score
turns
end

#
* *
at room36
score
turns
end

#
* *
notzero FLAG_SCORE_PLUS
clear FLAG_SCORE_PLUS
"^[The score has gone up by 10 points.]"
plus FLAG_SCORE 10

#
* *
eq FLAG_INV 1
set FLAG_INV
"^Type X DECK to display the contents of your deck."
;

Array EventTable table
#
n *
notzero FLAG_YESNO
at room11
present kitten
destroy kitten
swap chick chick_1
swap tree tree_1
"Kitten: Too bad! See you later."
anykey
desc

#
n *
notzero FLAG_YESNO
at room8
present seguin_upseting
"Seguin: Get out of my vegetable garden, then!"
done

#
n *
notzero FLAG_YESNO
at room18
present hole_1
done

#
n *
at room6
notzero FLAG_CORDE
"I can't, since the rope is holding me back."
done

#
n *
at room6
goto room9
desc

#
n *
at room7
present door_opened
goto room10
desc

#
n *
at room7
"I can't, since the door is closed."
done

#
n *
at room16
present door_opened
place door_opened room19
goto room19
desc

#
n *
at room16
"I can't, since the door is closed."
done

#
n *
at room18
goto room17
desc

#
n *
at room22
present boulder_1
"I can't, since the boulder is in the way."
done

#
n *
at room22
goto room21
desc

#
n *
at room28
present boulder_1
"I can't, since the boulder is in the way."
done

#
n *
at room28
goto room21
desc

#
n *
at room27
zero FLAG_PATH
set FLAG_PATH
swap track chamois
place boulder room24
swap sign sign_broken
create bone
"As Blanquette begins to ascend the mountain, a huge boulder comes loose, crushing everything in its path."
anykey
desc

#
n *
at room27
"The path is too dangerous to ascend it without an experienced guide."
done

#
s *
at room6
eq FLAG_CORDE 255
"I can't, since the rope is holding me back."
done

#
s *
at room6
goto room3
desc

#
s *
at room8
notzero FLAG_CORDE
"I can't, since the rope is holding me back."
done

#
s *
at room8
goto room5
desc

#
s *
at room12
"I can't, since the door is locked."
done

#
s *
at room19
present door_opened
place door_opened room16
goto room16
desc

#
s *
at room19
"I can't, since the door is closed."
done

#
s *
at room21
notzero FLAG_BOULDER
"I can't, since the boulder is in the way."
done

#
s *
at room21
goto room28
desc

#
s *
atgt room28
"You can't run away! You have to fight."
done

#
e *
at room6
eq FLAG_CORDE 1
"I can't, since the rope is holding me back."
done

#
e *
at room6
goto room7
desc

#
e *
at room7
"I can't, since the fence is in the way."
done

#
e *
present barrel_1
set FLAG_BARREL
done

#
e *
at room12
"I can't reach the window from here."
done

#
e *
present elves
"Elf: Hey! Blanquette, where are you going? Comes to dance with us."
"^Blanquette is dragged into the farandole, and comes out dazed."
done

#
e *
at room14
"I can't, since part of the bridge has been washed away by the river."
done

#
e *
at room18
zero FLAG_SHORTCUT
set FLAG_SHORTCUT
set FLAG_SCORE_PLUS
"I found a shortcut through a winding path covered in brush!"
anykey

#
e *
at room18
goto room15
desc

#
e *
at room21
set FLAG_DARK
goto room20
desc

#
e *
at room24
zero FLAG_SHORTCUT
set FLAG_SHORTCUT
set FLAG_SCORE_PLUS
"I found a shortcut through a narrow, low passage."
anykey

#
e *
at room24
goto room21
desc

#
w *
at room4
notzero FLAG_CORDE
let FLAG_CORDE 1

#
w *
at room4
goto room3
desc

#
w *
at room8
"I can't, since the fence is in the way."
done

#
w *
at room15
zero FLAG_SHORTCUT
set FLAG_SHORTCUT
set FLAG_SCORE_PLUS
"I found a shortcut through a winding path covered in brush!"
anykey

#
w *
at room15
goto room18
desc

#
w *
at room20
clear FLAG_DARK
goto room21
desc

#
w *
at room21
zero FLAG_SHORTCUT
set FLAG_SHORTCUT
set FLAG_SCORE_PLUS
"I found a shortcut through a narrow, low passage."
anykey

#
w *
at room21
goto room24
desc

#
w *
at room24
"I can't, since part of the bridge has been washed away by the river."
done

#
u barrel
present kitten_asleep
"I must wake Kitten first."
done

#
u barrel
present barrel
"Blanquette jumps on the barrel, peers out of the window on the opposite wall, then climbs back down."
done

#
u barrel
present barrel_1
set FLAG_BARREL
done

#
u window
present barrel_1
set FLAG_BARREL
done

#
u window
at room12
"I can't reach the window from here."
done

#
u river
at room14
"I don't jump as far as the springbok."
done

#
u river
at room24
"I don't jump as far as the springbok."
done

#
u *
at room7
goto room8
desc

#
u *
at room8
goto room7
desc

#
u *
at room2
notzero FLAG_CORDE
"I can't, since the rope is holding me back."
done

#
u *
at room2
goto room11
desc

#
u *
"Blanquette gives a kick."
done

#
enter hole
present cart
"I can't, since the hole is under the overturned cart."
done

#
enter hole
present hole
"I can't, since the hole is too narrow."
done

#
enter hole
present hole_1
"Underground voice: Did you do everything you intended to do around here? (Y/N)"
Let FLAG_YESNO 2
done

#
run *
atgt room28
"You can't run away! You have to fight."
done

#
x key
present salamander
"The key is at the bottom of the basin, among the dead leaves."
done

#
x key
present key
"A large antique key weakened by rust."
done

#
x key
present key_broken
"All that's left is to make an identical key."
done

#
x tambourine
present tambourine
"A circular percussion instrument with pairs of small metal jingles."
done

#
x dung
present dung
"A high-quality flytrap."
done

#
x dung
present dung_fly
"There's a lot of buzz around it!"
done

#
x bag
present owl
"The burlap sack is used as a blanket by the owl."
done

#
x bag
present bag_empty
"The bag is empty, and just needs to be filled."
done

#
x bag
present bag_flour
"The bag is full, and ready to go."
done

#
x torch
present torch_lit
"A flaming torch to see in the dark."
done

#
x torch
present torch_unlit
"It's just waiting to be lighted."
done

#
x book
present book
"It says: Great Kitchen Dictionary by Alexandre Dumas."
done

#
x bone
present bone
"A pierced bone from a goat's leg."
done

#
x sign
present sign_broken
"It seems suitable for firewood."
done

#
x deck
set FLAG_INV
"The deck contains:^ Basic rules"

#
x deck
notzero FLAG_CARD4
" [1] The Kitten card"

#
x deck
notzero FLAG_CARD2
" [2] The Toadstool card"

#
x deck
notzero FLAG_CARD1
" [3] The Four-leaf Clover card"

#
x deck
notzero FLAG_CARD3
" [4] The Venomous Snake card"

#
x deck
notzero FLAG_CARD5
" [5] The Morning Star card"

#
x deck
done

#
x card
present card0
"The card seems cursed, and I have no way of blessing it."
done

#
x card
present card1
"With this card added to your deck, the final battle will be in your favour."
done

#
x card
present card2
"With this card added to your deck, the final battle will be in your favour."
done

#
x card
present card3
"With this card added to your deck, the final battle will be in your favour."
done

#
x card
present card4
"With this card added to your deck, the final battle will be in your favour."
done

#
x card
present card5
"With this card added to your deck, you're ready for the final battle."
done

#
x *
"You can only usefully examine objects surrounded by (*).^Type HELP, please."
done

#
read rule
"It says: When Blanquette has to fight her destiny, choose an available card from your deck, and type the number associated with it."
done

#
read sign
at room3
zero FLAG_OBEDIENT
set FLAG_OBEDIENT
set FLAG_SCORE_PLUS

#
read sign
at room3
"You're obedient, which is good."
done

#
read sign
present sign
at room24
"It's says: Risk of falling rocks!"
done

#
read book
present book
drop book
get book
"There's a recipe for lamb cooked over a wood fire."
done

#
read boulder
present boulder_1
"It says: There must be another entrance somewhere."
done

#
get card
present card0
"I'd rather not, as it could have a negative effect on my deck."
done

#
get card
present card1
plus FLAG_NCARDS 1
destroy card1
set FLAG_CARD1
desc

#
get card
present card2
plus FLAG_NCARDS 1
destroy card2
set FLAG_CARD2
desc

#
get card
present card3
plus FLAG_NCARDS 1
destroy card3
set FLAG_CARD3
desc

#
get card
present card4
plus FLAG_NCARDS 1
destroy card4
set FLAG_CARD4
desc

#
get card
present card5
present torch_lit
create torch_lit
destroy card5
set FLAG_CARD5
desc

#
get key
at room13
present salamander
"I can't reach it from here."
"^Salamander: I'll get it for you if you bring me some flies to nibble on."
done

#
get key
present key_broken
notcarr key_broken
"I can leave it there, since I don't need it anymore."
done

#
get key
present key
get key
desc

#
get key
get key_broken
desc

#
get tambourine
present elves
"Elf: You can have it when the party's over."
done

#
get tambourine
get tambourine
desc

#
get dung
at room13
absent salamander
"I can leave it there, since I don't need it anymore."
done

#
get dung
present dung
get dung
desc

#
get dung
get dung_fly
desc

#
get bag
present owl
"I can't reach it from here."
done

#
get bag
present bag_empty
get bag_empty
desc

#
get bag
get bag_flour
desc

#
get torch
present torch_unlit
present hermit
"Hermit: Hey! Nobody, you know well that I hate anybody touching my stuff."
done

#
get torch
at room20
present torch_lit
"I can leave it there, since I don't need it anymore."
done

#
get torch
present torch_unlit
get torch_unlit
desc

#
get torch
get torch_lit
desc

#
get book
get book
desc

#
get bone
notzero FLAG_BOULDER
present bone
notcarr bone
"I can leave it there, since I don't need it anymore."
done

#
get bone
get bone
desc

#
get sign
present sign_broken
present boulder
"I can't, since the sign is stuck under the boulder."
done

#
get sign
present sign_broken
get sign_broken
desc

#
get *
"You can only get objects surrounded by (*).^Type HELP, please."
done

#
drop deck
"You can't, since Blanquette need it for the final fight."
done

#
drop key
present key
drop key
desc

#
drop key
drop key_broken
desc

#
drop tambourine
drop tambourine
desc

#
drop dung
carried dung
present mule_fly
swap mule_fly mule
destroy dung
create dung_fly
desc

#
drop dung
present dung
drop dung
desc

#
drop dung
drop dung_fly
desc

#
drop bag
present bag_empty
drop bag_empty
desc

#
drop bag
drop bag_flour
desc

#
drop torch
at room20
present torch_unlit
drop torch_unlit
place torch_unlit room22
ok

#
drop torch
at room28
present torch_unlit
drop torch_unlit
place torch_unlit room22
ok

#
drop torch
present torch_unlit
drop torch_unlit
desc

#
drop torch
at room28
drop torch_lit
place torch_lit room22
ok

#
drop torch
drop torch_lit
desc

#
drop book
at room20
drop book
place book room22
ok

#
drop book
at room28
drop book
place book room22
ok

#
drop book
drop book
desc

#
drop bone
at room20
drop bone
place bone room22
ok

#
drop bone
at room28
drop bone
place bone room22
ok

#
drop bone
drop bone
desc

#
drop sign
present sign_broken
at room20
drop sign_broken
place sign_broken room22
ok

#
drop sign
present sign_broken
drop sign_broken
desc

#
drop sign
drop sign
desc

#
fill bag
at room19
present bag_flour
"The bag is already full of flour."
done

#
fill bag
at room19
present bag_empty
drop bag_empty
get bag_empty
swap bag_empty bag_flour
"You fill the bag with the flour from the millstone."
done

#
fill bag
notat room19
present bag_empty
drop bag_empty
get bag_empty
"There isn't anything obvious with which to fill it."
done

#
empty bag
present bag_empty
"The bag is already empty."
done

#
empty bag
present bag_flour
drop bag_flour
get bag_flour
swap bag_flour bag_empty
ok

#
wear tambourine
drop tambourine
get tambourine
"I'd rather not. I'll never again be restrained by a collar and leash, rope or chain."
done

#
give deck
"You can't, since Blanquette need it for the final fight."
done

#
give dung
present dung_fly
present salamander
drop dung_fly
get dung_fly
destroy salamander
destroy dung_fly
create dung
"The flies rush to the salamander, which swallows them at once. The salamander vanishes under the leaves after having brought back the key."
anykey
desc

#
give tambourine
present mule
drop tambourine
get tambourine
set FLAG_SNAKE
swap snake card3
destroy mule
destroy cart
destroy tambourine
place mule_cart room16
"Mule: Thanks! This will make a pretty bell collar. See you later."
anykey
desc

#
give tambourine
present snake
drop tambourine
get tambourine
"Snake: I'm not a pet to be tied up or caged!"
done

#
give bag
present bag_empty
present mule_cart
drop bag_empty
get bag_empty
"But the bag is empty at the moment."
done

#
give bag
present mule_cart
drop bag_flour
get bag_flour
"Mule: PUT it in the cart."
done

#
give book
present hermit
drop book
plus FLAG_CAMPFIRE 1
destroy book
"Hermit: Thanks! Nobody. And what else?"
done

#
give sign
present hermit
drop sign_broken
plus FLAG_CAMPFIRE 1
destroy sign_broken
"Hermit: Thanks! Nobody. And what else?"
done

#
give *
"No one seems interested."
done

#
put bag
present bag_empty
drop bag_empty
get bag_empty
Let FLAG_PUTIN 2
"In what?"
done

#
put bag
drop bag_flour
get bag_flour
Let FLAG_PUTIN 2
"In what?"
done

#
cart *
notzero FLAG_PUTIN
carried bag_empty
present mule_cart
"But the bag is empty at the moment."
done

#
cart *
notzero FLAG_PUTIN
present mule_cart
destroy bag_flour
destroy mule_cart
set FLAG_SCORE_PLUS
"Mule: Thanks! See you later."
anykey
desc

#
talk kitten
at room11
present kitten
Let FLAG_YESNO 2
"Kitten: Hi Blanquette, I can't see my beloved sister from up here. Do you know where she's hidden? (Y/N)"
done

#
talk seguin
present seguin_milking
clear FLAG_CORDE
destroy rope
destroy seguin_milking
"Seguin: Congratulations Blanquette, your milk is of very good quality. Now that my bucket is full, I'm going to take a little nap."
anykey
desc

#
talk seguin
present seguin_upseting
zero FLAG_SEGUIN
set FLAG_SEGUIN
"Seguin: A rabbit stole my carrots again! Hey! Blanquette, what's going on?^^*: I want to go to the mountain, Mr Seguin.^^Seguin: But don't you know that there's a wolf in the mountain? What will you do when he comes?^^*: It doesn't matter, Mr Seguin, let me go to the mountain.^^Seguin: I will lock you in the stable, and you will stay there!^"

#
talk seguin
present seguin_upseting
"Seguin: Did you do everything you intended to do around here? (Y/N)"
Let FLAG_YESNO 2
done

#
talk rooster
present rooster
swap rooster rooster_1
place seguin_upseting room8
"Rooster: Hi Blanquette, the wild freedom is on the other side of the wall. LISTEN to the mountain breath, and you will know the great price of freedom."
"^*: How good it must be up there! Without fences or rope that skin my neck."
"^Rooster: By the way, I saw Mr Seguin in the vegetable garden."
anykey
desc

#
talk rooster
present rooster_1
"Rooster: COCK-A-DOODLE-DO!"
done

#
talk chick
present chick
"Chick: Hi Blanquette, if you see my brother Kitten, don't tell him where I'm hiding!"
done

#
talk chick
present chick_1
"Chick: CHEEP-CHEEP!"
done

#
talk salamander
present salamander
"Salamander: Hi Blanquette, you shouldn't be here, Mr Seguin will be worried."
done

#
talk elves
present elves
"Elf: Hi Blanquette, we're celebrating the Shepherd's day, come to dance with us."
"^Blanquette is dragged into the farandole, and comes out dazed."
done

#
talk mule
present mule_fly
"Mule: Hi Blanquette, I have to fetch the wrecked hay wagon, but a nasty snake is in my way. Oh, if only I had my bell collar to scare him off!"
done

#
talk mule
present mule
"Mule: Hi Blanquette, I have to fetch the wrecked hay wagon, but a nasty snake is in my way. Oh, if only I had my bell collar to scare him off!"
done

#
talk mule
present mule_cart
"Mule: Hi Blanquette, I have to bring a bag of flour to Mr Seguin. If you have one, PUT it in the cart."
done

#
talk snake
present snake
"Snake: Hi blanquette, the mule, far too clumsy to take the shortcut, nearly ran me over as I passed. Next time, I'll bite her!"
done

#
talk owl
present owl
"Owl: Hi blanquette, I see little rabbits grazing on the clover outside the windmill, but I'm too old to chase them."
done

#
talk rabbit
present rabbit_leisurely
present door_opened
destroy rabbit_leisurely
place rabbit_fearful room19
set FLAG_RABBIT
"The scared rabbit runs inside windmill at high speed."
anykey
desc

#
talk rabbit
present rabbit_leisurely
destroy rabbit_leisurely
"The scared rabbit runs away at high speed."
anykey
desc

#
talk rabbit
present rabbit_fearful
present door_opened
destroy rabbit_fearful
clear FLAG_RABBIT
"The scared rabbit runs away at high speed."
anykey
desc

#
talk rabbit
present rabbit_fearful
destroy rabbit_fearful
destroy owl
place card1 room16
let FLAG_STORM 1
"The frightened rabbit runs away at high speed, and hits the closed door.^^Owl: Hey! what's going on?^^The owl dives on the stunned rabbit, and carries its prey to safety.^^The burlap bag fell off the roof framework."
anykey
desc

#
talk hermit
at room21
present hermit
zero FLAG_NOBODY
set FLAG_NOBODY
"Hermit: Hi Nobody, bring me some stale bread, wood and paper so I can make a campfire toast.^^*: I'm not Nobody, I'm Blanquette!^^Hermit: Ha ha ha! And I'm Polypheme! What a joker this Nobody."
done

#
talk hermit
at room21
present hermit
"Hermit: Hi Nobody, bring me some stale bread, wood and paper so I can make a campfire toast."
done

#
talk ant
present ant
"Ant: Hi Blanquette, the hermit and his lamb trample us every time they come out of the cave. If you find anything to block the entrance, let me know where by blowing a loud whistle."
done

#
talk lamb
present lamb
zero FLAG_LAMB
set FLAG_LAMB
set FLAG_SCORE_PLUS

#
talk lamb
present lamb
"Robin: Hi Blanquette, I'm sure the hermit wants to eat me to atone for his sins!"
done

#
talk lamb
present lamb_1
"Lamb: BAA MAA!"
done

#
talk chamois
present chamois
notzero FLAG_CARD5
dropall
get deck
clear FLAG_CARD5
goto room29
"Chamois: It's time to face your destiny, Blanquette.^^The deck of cards shakes and lights up, the Morning Star card springs up to join the firmament. It's pitch-dark now."
anykey
desc

#
talk chamois
present chamois
"Chamois: Hi Blanquette, find the Morning Star card, and I'll lead you on the path to your destiny.^Don't forget to SAVE before the final battle."
done

#
talk moth
notzero FLAG_BOULDER
at room20
absent torch_lit
"Moth: ..."
done

#
talk moth
at room20
absent torch_lit
"Moth: Hi Blanquette, I saw a lamb runs away at high speed to the west, south and west again."
done

#
talk *
"There's no reply."
done

#
wake kitten
present kitten_asleep
notzero FLAG_LICK
swap kitten_asleep barrel
"Kitten stretches, licks his paw, and disgusted by the taste of salt, runs away through the window."
anykey
desc

#
wake kitten
present kitten_asleep
"Kitten stretches, licks his paw to clean his hair, and falls back to sleep."
done

#
wake hermit
present hermit_1
"Nothing can wake him up."
done

#
eat daisy
present daisy
destroy daisy
minus FLAG_EAT 1
set FLAG_SCORE_PLUS
desc

#
eat carrot
present carrot
destroy carrot
minus FLAG_EAT 1
set FLAG_SCORE_PLUS
desc

#
eat almond
present almond
destroy almond
minus FLAG_EAT 1
set FLAG_SCORE_PLUS
desc

#
eat salt
present salt_stone
"It's not for eating, it's for licking."
done

#
eat *
"It's not my favourite meal."
done

#
lick salt
present salt_stone
set FLAG_SALT
"My tongue is now full of salt."
done

#
lick kitten
present kitten_asleep
notzero FLAG_SALT
set FLAG_LICK

#
lick kitten
present kitten_asleep
"Kitten purrs with pleasure in his sleep."
done

#
drink *
at room14
"Blanquette quenches its thirst, and splashes with fresh water."
done

#
drink *
at room24
"Blanquette quenches its thirst, and splashes with fresh water."
done

#
drink *
at room23
present lamb
"The lamb seems hypnotized by the water's reflection of Blanquette.^^Robin: Hey! Blanquette, what big eyes you have! What big ears you have! What big teeth you have!^^The scared lamb runs away at high speed."
destroy lamb
place lamb_1 room21
plus FLAG_CAMPFIRE 1
anykey
desc

#
drink *
at room23
"Blanquette quenches its thirst, and splashes with fresh water."
done


#
drink *
"There's nothing suitable to drink here."
done

#
open door
present door_opened
"The door is already open."
done

#
open door
present door_locked
"I can't, since the door is locked."
done

#
open door
present door_closed
swap door_closed door_opened
desc

#
close door
present door_closed
"The door is already closed."
done

#
close door
present door_locked
"The door is already closed."
done

#
close door
at room7
present door_opened
"Seguin: Hey! Blanquette, leave this door open."
done

#
close door
present door_opened
swap door_opened door_closed
desc

#
lock door
present door_locked
"The door is already locked."
done

#
lock door
present door_opened
"I can't, since the door is opened."
done

#
lock door
present door_closed
notcarr key
"I need a key."
done

#
lock door
present door_closed
swap door_closed door_locked
desc

#
unlock door
present door_locked
notcarr key
"I need a key."
done

#
unlock door
at room16
present door_locked
swap door_locked door_closed
swap key key_broken
desc

#
rub card
present card0
swap card0 card4
desc

#
rub *
"I achieve nothing by this."
done

#
burn torch
present torch_unlit
present campfire
drop torch_unlit
get torch_unlit
swap torch_unlit torch_lit
place hermit_1 room21
swap campfire ashes
desc

#
burn torch
present torch_unlit
"There's nothing here to light it."
done

#
burn skeleton
present skeleton
"Renaude didn't want to be cremated."
done

#
burn *
"This dangerous act would achieve little."
done

#
extinguish torch
present torch_lit
"The flame dies, and is reborn again."
done

#
extinguish campfire
present campfire
"The flame dies, and is reborn again."
done

#
break rope
present rope
"I can't, since the rope is strong and the knot very tight."
done

#
break *
"I could get hurt."
done

#
attack *
atgt room29
"Use your cards to fight."
done

#
attack *
"Violence isn't the answer to this one."
done

#
move cart
present cart
"I don't have enough strength to lift it."
done

#
move statue
present statue
"I don't have enough strength to move it."
done

#
move barrel
present kitten_asleep
"I must wake Kitten first."
done

#
move barrel
present barrel
absent barrel_1
swap barrel barrel_1
"The barrel rolls under the window."
anykey
desc

#
move kitten
present kitten_asleep
"Violence isn't the answer to this one."
done

#
move boulder
present boulder
"I don't have enough strength to move it."
done

#
move boulder
present boulder_1
"I don't have enough strength to move it."
done

#
move boulder
at room21
notzero FLAG_BOULDER
"I don't have enough strength to move it."
done

#
move skeleton
present skeleton
"Leave her in peace. The vultures will take care of it."
done

#
play tambourine
at room17
present snake
carried tambourine
zero FLAG_JINGLE
set FLAG_SCORE_PLUS
set FLAG_JINGLE

#
play tambourine
at room17
present snake
drop tambourine
get tambourine
destroy snake
"~TING-A-LING!~^^At the sound of the jingle, the snake runs away at high speed."
anykey
desc

#
play tambourine
drop tambourine
get tambourine
"~TING-A-LING!~"
done

#
play bone
present bone
drop bone
get bone
"Try to BLOW into it instead."
done

#
1 *
atgt room29
notzero FLAG_CARD4
clear FLAG_CARD4
let FLAG_PLAY 1
minus FLAG_NCARDS 1
" [1] The Kitten card"
done

#
1 *
atgt room29
zero FLAG_CARD4
"This card is not in your deck."
done

#
2 *
atgt room29
notzero FLAG_CARD2
clear FLAG_CARD2
let FLAG_PLAY 2
minus FLAG_NCARDS 1
" [2] The Toadstool card"
done

#
2 *
atgt room29
zero FLAG_CARD2
"This card is not in your deck."
done

#
3 *
atgt room29
notzero FLAG_CARD1
clear FLAG_CARD1
let FLAG_PLAY 3
minus FLAG_NCARDS 1
" [3] The Four-leaf Clover card"
done

#
3 *
atgt room29
zero FLAG_CARD1
"This card is not in your deck."
done

#
4 *
atgt room29
notzero FLAG_CARD3
clear FLAG_CARD3
let FLAG_PLAY 4
minus FLAG_NCARDS 1
" [4] The Venomous Snake card"
done

#
4 *
atgt room29
zero FLAG_CARD3
"This card is not in your deck."
done

#
blow bone
at room24
present boulder
drop bone
get bone
destroy boulder
swap ant boulder_1
set FLAG_BOULDER
"~TWEET TWEET TWEET!~"
"^The ground darkened and began to move, dragging the rock with it.^^Ant: Thanks! See you later."
anykey
desc

#
blow bone
present ant
drop bone
get bone
"~TWEET TWEET TWEET!~"
"^Ant: Blow a loud whistle where you find anything to block the entrance."
done

#
blow bone
drop bone
get bone
"~TWEET TWEET TWEET!~"
done

#
blow *
"~TWEET!~"
done

#
dig *
present hole
absent cart
swap hole hole_1
desc

#
dig *
present skeleton
"Renaude didn't want to be buried."
done

#
dig *
"Digging would achieve nothing here."
done

#
dance *
present elves
"Blanquette is dragged into the farandole, and comes out dazed."
done

#
dance *
"Blanquette move in a graceful and rhythmical way."
done

#
swim *
at room14
"The river's current is too strong."
done

#
swim *
at room24
"The river's current is too strong."
done

#
listen dung
present dung_fly
"There's a lot of buzz around it!"
done

#
listen *
at room9
zero FLAG_LISTEN
set FLAG_LISTEN
set FLAG_SCORE_PLUS

#
listen *
at room9
"I hear a wolf howling in the distance."
done

#
listen *
present hole
"Underground voice: BAA MAA!"
done

#
listen *
present hole_1
"~BAA MAA!~"
done

#
listen *
"I hear nothing unexpected."
done

#
smell dung
present dung
"It doesn't smell like roses."
done

#
smell dung
present dung_fly
"It doesn't smell like roses."
done

#
smell *
at room6
"A subtle flowery fragrance."
done

#
smell *
present campfire
"Robin's body odour."
done

#
smell *
"I smell nothing unexpected."
done

#
sleep *
"I'm not feeling especially drowsy."
done

#
wait *
"Nothing happens."
done

#
ask *
"Use TALK to communicate with characters."
done

#
use *
"You don't need to use this verb in this game."
done

#
yes *
notzero FLAG_YESNO
at room11
present kitten
destroy kitten
swap card0 chick
swap tree tree_1
"Kitten: Thanks! See you later."
anykey
desc

#
yes *
notzero FLAG_YESNO
at room8
present seguin_upseting
place door_locked room12
place salt_stone room12
clear FLAG_LICK
goto room12
desc

#
yes *
notzero FLAG_YESNO
at room18
dropall
get deck
goto room20
set FLAG_DARK
clear FLAG_SHORTCUT
clear FLAG_BOULDER
clear FLAG_PATH
clear FLAG_NOBODY
clear FLAG_CAMPFIRE
"Blanquette is sucked down the rabbit hole, and falls back into a cave, onto a lamb that cushions her fall."
anykey
desc

#
help *
cls
"The game expect commands in the form of VERB NOUN (not case-sensitive).^^Basic commands:^L or R to refresh the location display.^N, S, E, W, U & D to move around.^I to display what you're carrying and wearing.^X to examine an object.^GET & DROP to pick up or drop an object.^TALK to communicate with a character.^VERBS to display commands needed to complete the game.^SAVE & RESTORE to save or restore the game.^^Interact only with objects displayed after: I notice.^EXAMINE (X) and GET are only useful for objects surrounded by (*) e.g. A *key*."
anykey
desc

#
about *
"Mr Seguin's Goat (c) 2023 Lionel Ange.^Based on a short story in Letters from My Windmill (1869), by the French writer and playwright Alphonse Daudet.^Playtesting by Nobody and Polypheme.^^Release 1.2 / Serial number 231215 / zQuill 1.4^The game code is compatible with The Quill (Gilsoft).^^This game was created for Text Adventure Literacy Jam in May 2023."
done

#
verbs *
"Some useful commands:^L (look), I (inventory)^X (examine), READ^N, S, E, W, U, D, CLIMB, JUMP, GO^TALK, GIVE, WAKE, EAT, DRINK, LICK, BLOW^GET, DROP, PUT, WEAR, REMOVE, FILL, EMPTY^OPEN, CLOSE, UNLOCK, MOVE, DIG^LIGHT, EXTINGUISH, CLEAN, PLAY, LISTEN^SAVE, RESTORE, YES, NO, SCORE^HELP, ABOUT, VERBS^^And other commands (or synonyms) that you will discover by yourself."
done

#
i *
zero FLAG_INV
Let FLAG_INV 1

#
i *
inven

#
l *
desc

#
script *
script
done

#
score *
score
turns

#
q *
quit
turns
end

#
save *
atgt room28
"It's too late to save. Blanquette must face her destiny."
done

#
save *
save

#
load *
load
;
